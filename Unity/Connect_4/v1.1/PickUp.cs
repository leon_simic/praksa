using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    Rigidbody rb;
    Vector3 mouseOffset;
    float mouseZ;

    float rotationSpeed = 400f;
    bool rotateCoin;
    bool hasCollided;
    bool moveCoin = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }
    void OnMouseDown()
    {
        mouseZ = Camera.main.WorldToScreenPoint(transform.position).z;
        mouseOffset = transform.position - GetMouseAsWorldPoint();
        
        rotateCoin = true;
    }

    void Update()
    {
        if (rotateCoin && transform.eulerAngles.x < 89 && !hasCollided)
        {
            transform.Rotate(rotationSpeed * Time.deltaTime * Vector3.right);

            if (transform.eulerAngles.x > 89)
                transform.rotation = Quaternion.Euler(90, 180, 0);
        }

        RaycastHit hit;
        Ray coinRay = new Ray(transform.position, Vector3.forward * 10);

        Debug.DrawRay(transform.position, Vector3.forward * 10);

        if (Physics.Raycast(coinRay, out hit, 100))
        {
            if (hit.collider.tag == " " )
            {

            }
        }
    }

    void FixedUpdate()
    {
        if (moveCoin)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        hasCollided = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        hasCollided = false;
    }

    
    void OnMouseUp()
    {
        rb.isKinematic = true;
        rotateCoin = false;
        rb.isKinematic = false;
    }

    Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = mouseZ;
       
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        transform.position = GetMouseAsWorldPoint() + mouseOffset;
        //Debug.Log("x " + transform.position.x + " y " + transform.position.y);
        if ((transform.position.x >= -4 || transform.position.x <= 4.2) && (transform.position.y >= 9.3 || transform.position.y <= 10.1))
        {
            moveCoin = true;
            //FixedUpdate();
        }
    }

}
