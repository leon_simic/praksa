using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject[] Player1Coins;
    public GameObject[] Player2Coins;
    public bool player1Turn = true;
    public GameObject mainCamera;
    public Light mainLight;

    int[,] boardState;
    bool rotateCamera1;

    void Start()
    {
        boardState = new int[6, 7];    
    }

    public void SelectColumn(int column)
    {
        TakeTurn(column);
        TakeTurn(column);
        //Debug.Log("Chosen column: " + column);
    }

    void TakeTurn(int column)
    {
        if (player1Turn)
        {
            foreach (var coin in Player2Coins)
            {
                coin.GetComponent<PickUpAndPlace>().isEnabled = false;
            }
        }

        UpdateBoardState(column);
        player1Turn = false;
        rotateCamera1 = true;

        if (!player1Turn)
        {
            foreach (var coin in Player2Coins)
            {
                coin.GetComponent<PickUpAndPlace>().isEnabled = true;
            }

            foreach (var coin in Player1Coins)
            {
                coin.GetComponent<PickUpAndPlace>().isEnabled = false;
            }
        }

        player1Turn = true;
        rotateCamera1 = true;


        //for (int i = 0; i < 6; i++)
        //{
        //    for (int j = 0; j < 7; j++)
        //    {
        //        Debug.Log($"{i},{j}: {boardState[i, j]}");
        //    }
        //}
    }

    void Update()
    {
        if (rotateCamera1)
        {
            mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 1.5f);
            mainLight.transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime * 1.5f);

            if (mainCamera.transform.rotation.eulerAngles.y >= 180 && mainCamera.transform.rotation.eulerAngles.y <= 183)
            {
                mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);
                mainLight.transform.rotation = Quaternion.Euler(120, 20, 0);
                rotateCamera1 = false;
            }
        }

        //if (rotateCamera1 || rotateCamera2)
        //{
        //    mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 0.9f);
        //    mainLight.transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime * 0.9f);

        //    if (rotateCamera2)
        //    {
        //        if (mainCamera.transform.rotation.eulerAngles.y <= 180 && mainCamera.transform.rotation.eulerAngles.y >= 176)
        //        {
        //            mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);
        //            mainLight.transform.rotation = Quaternion.Euler(120, 20, 0);
        //            rotateCamera2 = false;
        //        }
        //    }
        //    else if (rotateCamera1)
        //    {
        //        if (mainCamera.transform.rotation.eulerAngles.y <= 0 && mainCamera.transform.rotation.eulerAngles.y >= -4)
        //        {
        //            mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
        //            mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);
        //            rotateCamera1 = false;
        //        }
        //    }
        //}
    }

    bool UpdateBoardState(int column)
    {
        for (int row = 0; row < 6; row++)
        {
            if (boardState[row, column] == 0)
            {
                if (player1Turn)
                    boardState[row, column] = 1;
                else
                    boardState[row, column] = 2;

                return true;
            }
        }
        return false;
    }
}
