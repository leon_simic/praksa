using UnityEngine;

public class PickUpAndPlace : MonoBehaviour
{
    public bool isEnabled = true;
    public GameManager gameManager;

    bool _mouseActive;
    bool _aboveEnterPoint;
    int _column;

    float _elapsedTime;
    float _percentageComplete;

    Vector3 _mouseOffset;
    Vector3 _startPosition;
    float _mouseZ;

    void OnMouseDown()
    {
        _mouseZ = Camera.main.WorldToScreenPoint(transform.position).z;
        _mouseOffset = transform.position - GetMouseAsWorldPoint();

        _mouseActive = true;
    }

    private void OnMouseUp()
    {
        _mouseActive = false;

        _startPosition = transform.position;
    }

    Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mouseZ;
       
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        _column = (int)(transform.position.x + 3.6);

        if (_column == 7)
            _column = 6;

        if (_column >= 0 && _column <= 6)
        {
            if (transform.position.y >= 8.7 && transform.position.y <= 11)
                _aboveEnterPoint = true;
        }

        if (isEnabled)  
            transform.position = GetMouseAsWorldPoint() + _mouseOffset;
    }

    void Update()
    {
        if (!_mouseActive && _aboveEnterPoint)
        {
            Vector3 endPosition0 = new Vector3(-3.89f, 9.7f, 0);
            Vector3 offset = new Vector3(1.211f, 0, 0);

            float _snapDuration = 0.1f;
            _elapsedTime += Time.deltaTime;
            _percentageComplete = _elapsedTime / _snapDuration; 

            if(_percentageComplete >= 0 && _percentageComplete <= 1f)
            {
                FindObjectOfType<GameManager>().SelectColumn(_column);
                switch (_column)
                {
                    case 0:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 1:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 2:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + 2 * offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 3:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + 3 * offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 4:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + 4 * offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 5:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + 5 * offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                    case 6:
                        transform.position = Vector3.Lerp(_startPosition, endPosition0 + 6 * offset, _percentageComplete);
                        if (_percentageComplete >= 0.85)
                            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                        break;
                }
            }
        }
    }
}
