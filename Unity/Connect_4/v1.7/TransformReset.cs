using UnityEngine;

public abstract class TransformReset : MonoBehaviour
{
    public Quaternion rotation;
    public Vector3 position;

    void Awake()
    {
        rotation = transform.rotation;
        position = transform.position;
    }

    public void ResetTransform()
    {
        transform.position = position;
        transform.rotation = rotation;
    }
}
