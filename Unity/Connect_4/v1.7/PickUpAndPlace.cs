using System;
using UnityEngine;

public class PickUpAndPlace : TransformReset
{
    public bool isEnabled = true;
    public int column = -1;

    bool _mouseActive;
    bool _aboveEnterPoint;

    CoinRotate _coinRotate;
    GameManager _gameManager;

    float _elapsedTime;
    float _percentageComplete;
    float _mouseZ;

    Vector3 _mouseOffset;
    Vector3 _startCorrectPosition;
    Vector3 _startPosition;
    Quaternion _startRotation;

    void Start()
    {
        _coinRotate = GetComponent<CoinRotate>();
        _gameManager = FindObjectOfType<GameManager>();
        _startPosition = transform.position;
        _startRotation = transform.rotation;
    }

    public void DisableCoinRotation()
    {
        _coinRotate.enabled = false;
    }

    public void EnableCoinRotation()
    {
        _coinRotate.enabled = true;
    }

    void OnMouseDown()
    {
        _mouseZ = Camera.main.WorldToScreenPoint(transform.position).z;
        _mouseOffset = transform.position - GetMouseAsWorldPoint();

        _mouseActive = true;
    }

    void OnMouseUp()
    {
        _mouseActive = false;

        _startCorrectPosition = transform.position;

        if (isEnabled)
            ResetCoinPosition();
        
        if (_aboveEnterPoint)
        {
            _gameManager.SetCurrentCoin(this);

            if (_gameManager.IsColumnFull(column))
            {
                Debug.Log("Full column");
                ResetCoinPosition();
            }
            else
            {
                isEnabled = false;
                _coinRotate.enabled = false;

                if (_gameManager.state == GameManager.State.PLAYER1TURN)
                {
                    _gameManager.player1Coins.Remove(this);
                    _gameManager.tempPlayer1Coins.Add(this);
                    _gameManager.DisableCurrentPlayer();
                }
                else if (_gameManager.state == GameManager.State.PLAYER2TURN)
                {
                    _gameManager.player2Coins.Remove(this);
                    _gameManager.tempPlayer2Coins.Add(this);
                    _gameManager.DisableCurrentPlayer();
                }
            }
        }
    }

    Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mouseZ;
       
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        column = (int)(transform.position.x + 3.6);

        if (column == 7)
            column = 6;

        if (column >= 0 && column <= 6)
        {
            if (transform.position.y >= 8.7 && transform.position.y <= 11)
                _aboveEnterPoint = true;
        }

        if (isEnabled)  
            transform.position = GetMouseAsWorldPoint() + _mouseOffset;        
    }

    void ResetCoinPosition()
    {
        if (transform.rotation.eulerAngles.x > 10)
        {
            if ((_startPosition.z <= 0 && transform.position.z >= 0) || (_startPosition.z >= 0 && transform.position.z <= 0))
            {
                transform.position = _startPosition;
                transform.rotation = _startRotation;
            }
        }
    }

    void FixedUpdate()
    {
        if (!_mouseActive && _aboveEnterPoint)
        {
            Vector3 endPosition0 = new Vector3(-3.89f, 9.7f, 0);
            Vector3 offset = new Vector3(1.211f, 0, 0);

            float _snapDuration = 0.07f;
            _elapsedTime += Time.deltaTime;
            _percentageComplete = _elapsedTime / _snapDuration;

            if (_percentageComplete >= 1f)
                _aboveEnterPoint = false;

            if (_percentageComplete >= 0 && _percentageComplete <= 1f)
            {
                switch (column)
                {
                    case 0:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0, _percentageComplete);
                        break;
                    case 1:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + offset, _percentageComplete);
                        break;
                    case 2:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 2 * offset, _percentageComplete);
                        break;
                    case 3:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 3 * offset, _percentageComplete);
                        break;
                    case 4:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 4 * offset, _percentageComplete);
                        break;
                    case 5:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 5 * offset, _percentageComplete);
                        break;
                    case 6:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 6 * offset, _percentageComplete);
                        break;
                }

                if (_percentageComplete >= 0.85)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                }
            }
        }
    }
}
