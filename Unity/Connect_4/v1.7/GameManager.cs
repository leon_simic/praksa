using System;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject menuPanel;
    public GameObject menuButton;
    public GameObject player1WinPanel;
    public GameObject player2WinPanel;
    public GameObject playerWinPanel;
    public GameObject aiWinPanel;

    public List<PickUpAndPlace> player1Coins;
    public List<PickUpAndPlace> player2Coins;
    public List<PickUpAndPlace> tempPlayer1Coins;
    public List<PickUpAndPlace> tempPlayer2Coins;
    public GameObject mainCamera;
    public Light mainLight;
    public bool onePlayerMode;

    int[,] _boardState;
    PickUpAndPlace _currentCoin;
    Rigidbody _currentCoinRb;
    public bool _isCurrentPlayerEnabled;
    int counterForUpdateBoard = 0;
    int counterForDisablingPlayers = 0;

    public enum State { INIT, MENU, ONEPLAYER, TWOPLAYERS, PLAYER1TURN, PLAYER2TURN, PLAYER1WIN, PLAYER2WIN, PLAYERWIN, AIWIN }
    public State state;


    void Start()
    {
        _boardState = new int[6, 7];
        SwitchState(State.MENU);
    }

    void SwitchState(State newState)
    {
        Debug.Log("Switching to state: " + newState);
        EndState();
        state = newState;
        BeginState(newState);
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.INIT:
                ResetCoinsToStartPosition();
                ResetBoardState();

                tempPlayer1Coins = new List<PickUpAndPlace>();
                tempPlayer2Coins = new List<PickUpAndPlace>();

                counterForDisablingPlayers = 0;
                counterForUpdateBoard = 0;
                _currentCoin = null;
                EnablePlayer1();
                SwitchState(State.MENU);
                break;
            case State.MENU:
                menuPanel.SetActive(true);
                break;
            case State.ONEPLAYER:
                _isCurrentPlayerEnabled = true;

                break;
            case State.TWOPLAYERS:
                _isCurrentPlayerEnabled = true;
                onePlayerMode = false;

                if (mainCamera.transform.rotation.y != 0)
                {
                    mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
                    mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);
                }

                SwitchState(State.PLAYER1TURN);
                break;
            case State.PLAYER1TURN:
                break;
            case State.PLAYER2TURN:
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
        }
    }

    void ResetCoinsToStartPosition()
    {
        if (tempPlayer1Coins != null)
            player1Coins.AddRange(tempPlayer1Coins);
        if (player1Coins != null)
            foreach (var coin in player1Coins)
            {
                coin.ResetTransform();
            }

        if (tempPlayer2Coins != null)
            player2Coins.AddRange(tempPlayer2Coins);
        if (player2Coins != null)
            foreach (var coin in player2Coins)
            {
                coin.ResetTransform();
            }
    }

    void ResetBoardState()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                _boardState[i, j] = 0;
            }
        }
    }

    void EndState()
    {
        switch (state)
        {
            case State.MENU:
                menuPanel.SetActive(false);
                break;
            case State.ONEPLAYER:
                break;
            case State.TWOPLAYERS:
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
        }
    }

    void Update()
    {
        switch (state)
        {
            case State.MENU:
                break;
            case State.ONEPLAYER:
                break;
            case State.TWOPLAYERS:
                break;
            case State.PLAYER1TURN:                
                if (!onePlayerMode)
                {
                    if (!_isCurrentPlayerEnabled)
                    {
                        counterForDisablingPlayers++;

                        if (counterForDisablingPlayers == 1)
                            DisablePlayer1();
                    }

                    if (_currentCoin != null)
                        _currentCoinRb = _currentCoin.GetComponent<Rigidbody>();

                    if (_currentCoin != null && _currentCoin.transform.position.y < 8.7 && _currentCoinRb.velocity.magnitude == 0)
                    {
                        counterForUpdateBoard++;
                        if (counterForUpdateBoard == 1)
                        {
                            UpdateBoardState(_currentCoin.column);
                            _currentCoin.column = -1;
                        }
                        var winner = CheckForWin();
                        if (winner != 0)
                        {
                            if (winner == 1)
                                SwitchState(State.PLAYER1WIN);
                            else
                                SwitchState(State.PLAYER2WIN);
                        }
                        else if (RotateCameraAndLight1())
                        {
                            SwitchState(State.PLAYER2TURN);
                            counterForDisablingPlayers = 0;
                            counterForUpdateBoard = 0;
                            _currentCoin = null;
                            EnablePlayer2();
                        }
                    }
                }
                break;
            case State.PLAYER2TURN:
                if (!onePlayerMode)
                {
                    if (!_isCurrentPlayerEnabled)
                    {
                        counterForDisablingPlayers++;

                        if (counterForDisablingPlayers == 1)
                            DisablePlayer2();
                    }

                    if (_currentCoin != null)
                        _currentCoinRb = _currentCoin.GetComponent<Rigidbody>();

                    if (_currentCoin != null && _currentCoin.transform.position.y < 8.7 && _currentCoinRb.velocity.magnitude == 0)
                    {
                        counterForUpdateBoard++;
                        if (counterForUpdateBoard == 1)
                            UpdateBoardState(_currentCoin.column);

                        var winner = CheckForWin();
                        if (winner != 0)
                        {
                            if (winner == 1)
                                SwitchState(State.PLAYER1WIN);
                            else
                                SwitchState(State.PLAYER2WIN);
                        }
                        else if (RotateCameraAndLight2())
                        {
                            SwitchState(State.PLAYER1TURN);
                            counterForDisablingPlayers = 0;
                            counterForUpdateBoard = 0;
                            _currentCoin = null;
                            EnablePlayer1();
                        }
                    }
                }
                break;
            case State.PLAYER1WIN:
                break;
            case State.PLAYER2WIN:
                break;
            case State.PLAYERWIN:
                break;
            case State.AIWIN:
                break;
        }
    }

    public void DisableCurrentPlayer()
    {
        _isCurrentPlayerEnabled = false;
    }

    public void SetCurrentCoin(PickUpAndPlace coin)
    {
        _currentCoin = coin;
    }

    bool RotateCameraAndLight1()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= 178 && mainCamera.transform.rotation.eulerAngles.y <= 190)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);
            mainLight.transform.rotation = Quaternion.Euler(120, 20, 0);

            _isCurrentPlayerEnabled = true;

            return true;
        }

        return false;
    }

    bool RotateCameraAndLight2()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(-30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= -2 && mainCamera.transform.rotation.eulerAngles.y <= 6)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
            mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);

            _isCurrentPlayerEnabled = true;

            return true;
        }

        return false;
    }

    #region Enabling and Disabling players
    void EnablePlayer1()
    {
        Debug.Log("Enabled player 1");

        foreach (var coin in player1Coins)
        {
            coin.isEnabled = true;
            coin.EnableCoinRotation();
        }
    }

    void EnablePlayer2()
    {
        Debug.Log("Enabled player 2");

        foreach (var coin in player2Coins)
        {
            coin.isEnabled = true;
            coin.EnableCoinRotation();
        }
    }

    void DisablePlayer1()
    {
        Debug.Log("Disabled player 1");

        foreach (var coin in player1Coins)
        {
            coin.isEnabled = false;
            coin.DisableCoinRotation();
        }
    }

    void DisablePlayer2()
    {
        Debug.Log("Disabled player 2");

        foreach (var coin in player2Coins)
        {
            coin.isEnabled = false;
            coin.DisableCoinRotation();
        }
    }
    #endregion

    public bool UpdateBoardState(int column)
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] == 0)
                {
                    if (state == State.PLAYER1TURN)
                    {
                        //Debug.Log("player1turn");
                        _boardState[i, j] = 1;
                    }
                    else if (state == State.PLAYER2TURN)
                    {
                        //Debug.Log("player2turn");
                        _boardState[i, j] = 2;
                    }

                    Debug.Log("coin put " + i + ", " + j);
                    return true;
                }
            }
        }

        return false;
    }

    public void OnePlayerClicked()
    {
        SwitchState(State.ONEPLAYER);
    }

    public void TwoPlayerClicked()
    {
        SwitchState(State.TWOPLAYERS);
    }

    #region WinCheck
    int CheckForWin()
    {
        if (CheckRows() != 0)
        {
            Debug.Log("Winner rows " + CheckRows());
            return CheckRows();
        }

        if (CheckColumns() != 0)
        {
            Debug.Log("Winner columns " + CheckColumns());
            return CheckColumns();
        }

        if (CheckMinorDiagonals() != 0)
        {
            Debug.Log("Winner minor diagonal " + CheckMinorDiagonals());
            return CheckMinorDiagonals();
        }

        if (CheckMajorDiagonals() != 0)
        {
            Debug.Log("Winner major diagonal " + CheckMajorDiagonals());
            return CheckMajorDiagonals();
        }

        if (IsBoardFull())
        {
            Debug.Log("Board is full!");
        }

        return 0;
    }

    int CheckForAWinner(int[] coins)
    {
        int countA = 0;
        int countB = 0;

        for (int i = 0; i < coins.Length; i++)
        {
            if (coins[i] == 0)
            {
                countA = 0;
                countB = 0;
            }
            else if (coins[i] == 1)
            {
                countA++;
                countB = 0;
            }
            else
            {
                countB++;
                countA = 0;
            }

            if (countA >= 4)
                return 1;
            else if (countB >= 4)
                return 2;
        }

        return 0;
    }

    int CheckRows()
    {
        int[] coins = new int[7];

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    coins[j] = _boardState[i, j];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckColumns()
    {
        int[] coins = new int[6];

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (_boardState[j, i] != 0)
                    coins[j] = _boardState[j, i];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMinorDiagonals()
    {
        int[] coins = new int[6];

        int sum = 3;
        int newIndex = 0;
        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i + j == sum)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            sum++;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMajorDiagonals()
    {
        int[] coins = new int[6];
        int difference = 2;
        int newIndex = 0;

        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 5; i >= 0; i--)
            {
                for (int j = 6; j >= 0; j--)
                {
                    if (i - j == difference)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            difference--;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }
    #endregion

    public bool IsColumnFull(int column)
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 6;
    }

    bool IsBoardFull()
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 42;
    }

    public void GoToMenu()
    {
        SwitchState(State.INIT);
    }
}
