using UnityEngine;

public class PickUpAndPlace : MonoBehaviour
{
    public bool isEnabled = true;

    bool _mouseActive;
    bool _aboveEnterPoint;
    int _column;

    float _elapsedTime;
    float _percentageComplete;

    Vector3 _mouseOffset;
    Vector3 _startCorrectPosition;
    Vector3 _startPosition;
    float _mouseZ;

    void OnMouseDown()
    {
        _mouseZ = Camera.main.WorldToScreenPoint(transform.position).z;
        _mouseOffset = transform.position - GetMouseAsWorldPoint();

        _mouseActive = true;

        _startPosition = transform.position;
    }

    void OnMouseUp()
    {
        _mouseActive = false;

        _startCorrectPosition = transform.position;

        ResetCoinPosition();
    }

    Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mouseZ;
       
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        _column = (int)(transform.position.x + 3.6);

        if (_column == 7)
            _column = 6;

        if (_column >= 0 && _column <= 6)
        {
            if (transform.position.y >= 8.7 && transform.position.y <= 11)
                _aboveEnterPoint = true;
        }

        if (isEnabled)  
            transform.position = GetMouseAsWorldPoint() + _mouseOffset;
    }

    void ResetCoinPosition()
    {
        if (transform.rotation.eulerAngles.x > 10)
        {
            if (_startPosition.z <= 0 && transform.position.z >= 0)
                transform.position = new Vector3(0, transform.position.y, -2);
            else if (_startPosition.z >= 0 && transform.position.z <= 0)
                transform.position = new Vector3(0, transform.position.y, 2);
        }
    }

    void FixedUpdate()
    { 
        if (!_mouseActive && _aboveEnterPoint)
        {
            FindObjectOfType<GameManager>().DisableCurrentPlayer();

            Vector3 endPosition0 = new Vector3(-3.89f, 9.7f, 0);
            Vector3 offset = new Vector3(1.211f, 0, 0);

            float _snapDuration = 0.1f;
            _elapsedTime += Time.deltaTime;
            _percentageComplete = _elapsedTime / _snapDuration;

            if (_percentageComplete >= 1f)
                _aboveEnterPoint = false;

            if (_percentageComplete >= 0 && _percentageComplete <= 0.2f)
            {
                FindObjectOfType<GameManager>().SelectColumn(_column);
                FindObjectOfType<GameManager>().SetCurrentCoin(gameObject);
            }

            if (_percentageComplete >= 0 && _percentageComplete <= 1f)
            {
                switch (_column)
                {
                    case 0:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0, _percentageComplete);
                        break;
                    case 1:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + offset, _percentageComplete);
                        break;
                    case 2:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 2 * offset, _percentageComplete);
                        break;
                    case 3:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 3 * offset, _percentageComplete);
                        break;
                    case 4:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 4 * offset, _percentageComplete);
                        break;
                    case 5:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 5 * offset, _percentageComplete);
                        break;
                    case 6:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 6 * offset, _percentageComplete);
                        break;
                }

                if (_percentageComplete >= 0.85)
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
            }
        }
    }
}
