using UnityEngine;

public class CoinRotate : MonoBehaviour
{
    Rigidbody rb;

    Vector3 _angleVelocity;
    bool _rotateCoin;
    bool _hasCollided;
    float _rotationY;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _angleVelocity = new Vector3(180, rb.position.y, rb.position.z);
    }

    void OnMouseDown()
    {
        _rotateCoin = true;
        _rotationY = rb.rotation.eulerAngles.y;
    }


    void FixedUpdate()
    {
        Quaternion deltaRotation = Quaternion.Euler(_angleVelocity * Time.fixedDeltaTime * 1.8f);

        if (_rotateCoin && rb.rotation.eulerAngles.x < 89 && !_hasCollided)
        {
            rb.MoveRotation(rb.rotation * deltaRotation);
            
            if (rb.rotation.eulerAngles.x > 85)
            {
                rb.rotation = Quaternion.Euler(90, _rotationY, 0);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        _hasCollided = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        _hasCollided = false;
    }

    void OnMouseUp()
    {
        rb.isKinematic = true;
        _rotateCoin = false;
        rb.isKinematic = false;
    }
}
