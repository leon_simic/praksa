using UnityEngine;

public class PickUpAndPlace : TransformReset
{
    public int column = -1;
    public bool aboveEnterPoint;
    public bool isColumnFull;
    public bool isCompletelyEnabled = true;

    bool _mouseActive;

    CoinRotate _coinRotate;
    GameManager _gameManager;

    float _elapsedTime;
    float _percentageComplete;
    float _mouseZ;

    Vector3 _mouseOffset;
    Vector3 _startCorrectPosition;
    public Vector3 startPosition;
    Quaternion _startRotation;

    Vector3 endPosition0 = new Vector3(-3.89f, 9.7f, 0);
    Vector3 offset = new Vector3(1.211f, 0, 0);

    void Start()
    {
        _coinRotate = GetComponent<CoinRotate>();
        _gameManager = FindObjectOfType<GameManager>();
        startPosition = transform.position;
        _startRotation = transform.rotation;
    }

    public void DisableCoinRotation()
    {
        _coinRotate.enabled = false;
    }

    public void EnableCoinRotation()
    {
        _coinRotate.enabled = true;
    }

    void OnMouseDown()
    {
        _mouseZ = Camera.main.WorldToScreenPoint(transform.position).z;
        _mouseOffset = transform.position - GetMouseAsWorldPoint();

        _mouseActive = true;
    }

    void OnMouseUp()
    {
        _mouseActive = false;

        _startCorrectPosition = transform.position;

        if (!aboveEnterPoint && isCompletelyEnabled)
            if ((startPosition.z <= 0 && transform.position.z >= -1) || (startPosition.z >= 0 && transform.position.z <= 1))
                ResetCoinPosition();
        
        if (aboveEnterPoint)
        {
            _gameManager.SetCurrentCoin(this);

            if (isColumnFull)
            {
                Debug.Log("Full column");
                ResetCoinPosition();
            }
            else
            {
                if (transform.rotation.eulerAngles.x < 90)
                    transform.rotation = Quaternion.Euler(90, _coinRotate.startRotationY, 0);

                isCompletelyEnabled = false;
                _coinRotate.enabled = false;

                if (_gameManager.state == GameManager.State.PLAYER1TURN)
                {
                    _gameManager.DisableCurrentPlayer();
                    _gameManager.player1Coins.Remove(this);
                    _gameManager.tempPlayer1Coins.Add(this);
                }
                else if (_gameManager.state == GameManager.State.PLAYER2TURN)
                {
                    _gameManager.DisableCurrentPlayer();
                    _gameManager.player2Coins.Remove(this);
                    _gameManager.tempPlayer2Coins.Add(this);
                }
            }
        }
    }

    Vector3 GetMouseAsWorldPoint()
    {
        Vector3 mousePoint = Input.mousePosition;
        mousePoint.z = _mouseZ;
       
        return Camera.main.ScreenToWorldPoint(mousePoint);
    }

    void OnMouseDrag()
    {
        //column = (int)(transform.position.x + 3.6);

        //if (column == 7)
        //    column = 6;

        GetInputColumn();

        if (column != -1)
        {
            isColumnFull = _gameManager.IsColumnFull(column);

            //Debug.Log("column " + column);
            if (transform.position.y >= 8.9 && transform.position.y <= 11)
            {
                aboveEnterPoint = true;
                //Debug.Log("enter point set to true");
            }
        }

        if (isCompletelyEnabled)
            transform.position = GetMouseAsWorldPoint() + _mouseOffset;        
    }

    void GetInputColumn()
    {
        if (transform.position.x >= -4.1f && transform.position.x <= -3f)
            column = 0;
        else if (transform.position.x > -3f && transform.position.x <= -1.83f)
            column = 1;
        else if (transform.position.x > -1.83f && transform.position.x <= -0.85f)
            column = 2;
        else if (transform.position.x > -0.85f && transform.position.x <= 0.22f)
            column = 3;
        else if (transform.position.x > 0.22f && transform.position.x <= 1.28f)
            column = 4;
        else if (transform.position.x > 1.28f && transform.position.x <= 2.28f)
            column = 5;
        else if (transform.position.x > 2.28f && transform.position.x <= 3.6f)
            column = 6;
        else
            column = -1;
    }

    void ResetCoinPosition()
    {
        if (transform.rotation.eulerAngles.x > 10)
        {
            transform.position = startPosition;
            transform.rotation = _startRotation;
        }
    }

    void Update()
    {
        if (!_mouseActive && aboveEnterPoint && !isColumnFull)
        {
            float _snapDuration = 0.07f;
            _elapsedTime += Time.deltaTime;
            _percentageComplete = _elapsedTime / _snapDuration;

            if (_percentageComplete >= 0 && _percentageComplete <= 1f)
            {
                switch (column)
                {
                    case 0:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0, _percentageComplete);
                        break;
                    case 1:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + offset, _percentageComplete);
                        break;
                    case 2:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 2 * offset, _percentageComplete);
                        break;
                    case 3:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 3 * offset, _percentageComplete);
                        break;
                    case 4:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 4 * offset, _percentageComplete);
                        break;
                    case 5:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 5 * offset, _percentageComplete);
                        break;
                    case 6:
                        transform.position = Vector3.Lerp(_startCorrectPosition, endPosition0 + 6 * offset, _percentageComplete);
                        break;
                }

                if (_percentageComplete >= 0.91f)
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                }
            }
        }
        else
            _elapsedTime = 0f;
    }

    void LerpCoins(Vector3 startPosition, Vector3 vector3)
    {

    }

}
