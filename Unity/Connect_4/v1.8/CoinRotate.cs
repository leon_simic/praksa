using UnityEngine;

public class CoinRotate : TransformReset
{
    public float startRotationY;

    Rigidbody rb;

    Vector3 _angleVelocity;
    bool _rotateCoin;
    bool _hasCollided;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        _angleVelocity = new Vector3(180, rb.position.y, rb.position.z);
    }

    void OnMouseDown()
    {
        _rotateCoin = true;
        startRotationY = rb.rotation.eulerAngles.y;
    }

    void FixedUpdate()
    {
        Quaternion deltaRotation = Quaternion.Euler(1.8f * Time.fixedDeltaTime * _angleVelocity);

        if (_rotateCoin && rb.rotation.eulerAngles.x < 89 && !_hasCollided)
        {
            rb.MoveRotation(rb.rotation * deltaRotation);
            
            if (rb.rotation.eulerAngles.x > 85)
            {
                rb.rotation = Quaternion.Euler(90, startRotationY, 0);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        _hasCollided = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        _hasCollided = false;
    }

    void OnMouseUp()
    {
        rb.isKinematic = true;
        _rotateCoin = false;
        rb.isKinematic = false;
    }
}
