using System;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.UIElements;
using Random = System.Random;

public class GameManager : MonoBehaviour
{
    public GameObject menuPanel;
    public GameObject menuButton;
    public GameObject player1WinPanel;
    public GameObject player2WinPanel;
    public GameObject playerWinPanel;
    public GameObject aiWinPanel;
    public GameObject drawPanel;

    public List<PickUpAndPlace> player1Coins;
    public List<PickUpAndPlace> player2Coins;
    public List<PickUpAndPlace> tempPlayer1Coins;
    public List<PickUpAndPlace> tempPlayer2Coins;
    public GameObject mainCamera;
    public Light mainLight;
    public bool onePlayerMode;

    int[,] _boardState;
    PickUpAndPlace _currentCoin;
    Rigidbody _currentCoinRb;
    Rigidbody _currentAICoin;
    public bool _isCurrentPlayerEnabled;
    int _counterForUpdateBoard = 0;
    int _counterForDisablingPlayers = 0;
    int _counterForAITurn = 0;

    float _elapsedTime;
    float _percentageComplete;

    Vector3 endPosition0 = new Vector3(-3.89f, 9.7f, 0);
    Vector3 offset = new Vector3(1.211f, 0, 0);

    public enum State { INIT, MENU, ONEPLAYER, TWOPLAYERS, PLAYER1TURN, PLAYER2TURN, PLAYER1WIN, PLAYER2WIN, PLAYERWIN, AIWIN, DRAW }
    public State state;

    void Start()
    {
        _boardState = new int[6, 7];
        SwitchState(State.INIT);
    }

    void SwitchState(State newState)
    {
        EndState();
        state = newState;
        BeginState(newState);
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.INIT:
                ResetCoinsToStartPosition();
                ResetBoardState();

                tempPlayer1Coins = new List<PickUpAndPlace>();
                tempPlayer2Coins = new List<PickUpAndPlace>();

                _counterForDisablingPlayers = 0;
                _counterForUpdateBoard = 0;
                _currentCoin = null;

                SwitchState(State.MENU);
                break;
            case State.MENU:
                if (mainCamera.transform.rotation.y != 0)
                {
                    mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
                    mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);
                }

                menuPanel.SetActive(true);
                break;
            case State.ONEPLAYER:
                _isCurrentPlayerEnabled = true;
                onePlayerMode = true;

                _currentAICoin = player2Coins[0].gameObject.GetComponent<Rigidbody>();
                EnablePlayer1();
                SwitchState(State.PLAYER1TURN);
                break;
            case State.TWOPLAYERS:
                _isCurrentPlayerEnabled = true;
                onePlayerMode = false;

                EnablePlayer1();
                SwitchState(State.PLAYER1TURN);
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
            case State.DRAW:
                drawPanel.SetActive(true);
                menuButton.SetActive(true);
                break;
        }
    }

    void ResetCoinsToStartPosition()
    {
        if (tempPlayer1Coins != null)
            player1Coins.AddRange(tempPlayer1Coins);
        if (player1Coins != null)
            foreach (var coin in player1Coins)
            {
                coin.ResetTransform();
            }

        if (tempPlayer2Coins != null)
            player2Coins.AddRange(tempPlayer2Coins);
        if (player2Coins != null)
            foreach (var coin in player2Coins)
            {
                coin.ResetTransform();
            }
    }

    void ResetBoardState()
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                _boardState[i, j] = 0;
            }
        }
    }

    void EndState()
    {
        switch (state)
        {
            case State.MENU:
                menuPanel.SetActive(false);
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
            case State.DRAW:
                drawPanel.SetActive(false);
                menuButton.SetActive(false);
                break;
        }
    }

    void Update()
    {
        switch (state)
        {
            case State.PLAYER1TURN:

                if (!_isCurrentPlayerEnabled)
                {
                    _counterForDisablingPlayers++;

                    if (_counterForDisablingPlayers == 1)
                        DisablePlayer1();
                }

                if (_currentCoin != null)
                    _currentCoinRb = _currentCoin.GetComponent<Rigidbody>();

                if (_currentCoin != null && _currentCoin.transform.position.y < 8.7 && _currentCoinRb.velocity.magnitude >= 0 && _currentCoinRb.velocity.magnitude <= 1.5f)
                {
                    _currentCoin.enabled = false;
                    _currentCoin.aboveEnterPoint = false;

                    _counterForUpdateBoard++;
                    if (_counterForUpdateBoard == 1)
                    {
                        UpdateBoardState(_currentCoin.column);
                        Debug.Log("You put coin in column " + _currentCoin.column);

                        _currentCoin.column = -1;
                    }

                    var winner = CheckForWin();
                    if (winner != 0)
                    {
                        if (winner == 1)
                        {
                            if (onePlayerMode)
                                SwitchState(State.PLAYERWIN);
                            else
                                SwitchState(State.PLAYER1WIN);
                        }
                        else
                        {
                            if (onePlayerMode)
                                SwitchState(State.AIWIN);
                            else
                                SwitchState(State.PLAYER2WIN);
                        }
                    }
                    else if (!_currentCoin.isColumnFull && RotateCameraAndLight1())
                    {
                        SwitchState(State.PLAYER2TURN);
                        _counterForDisablingPlayers = 0;
                        _counterForUpdateBoard = 0;
                        _currentCoin = null;

                        if (!onePlayerMode)
                            EnablePlayer2();
                    }
                }
                break;
            case State.PLAYER2TURN:
                if (!onePlayerMode)
                {
                    if (!_isCurrentPlayerEnabled)
                    {
                        _counterForDisablingPlayers++;

                        if (_counterForDisablingPlayers == 1)
                            DisablePlayer2();
                    }

                    if (_currentCoin != null)
                        _currentCoinRb = _currentCoin.GetComponent<Rigidbody>();

                    if (_currentCoin != null && _currentCoin.transform.position.y < 8.7 && _currentCoinRb.velocity.magnitude >= 0 && _currentCoinRb.velocity.magnitude <= 1.5f)
                    {
                        _currentCoin.enabled = false;
                        _currentCoin.aboveEnterPoint = false;

                        _counterForUpdateBoard++;
                        if (_counterForUpdateBoard == 1)
                        {
                            UpdateBoardState(_currentCoin.column);
                        }

                        var winner = CheckForWin();
                        if (winner != 0)
                        {
                            if (winner == 1)
                                SwitchState(State.PLAYER1WIN);
                            else
                                SwitchState(State.PLAYER2WIN);
                        }
                        else if (!_currentCoin.isColumnFull && RotateCameraAndLight2())
                        {
                            SwitchState(State.PLAYER1TURN);
                            _counterForDisablingPlayers = 0;
                            _counterForUpdateBoard = 0;
                            _currentCoin = null;
                            EnablePlayer1();
                        }
                    }
                }
                else
                {
                    DisablePlayer2();
                    _counterForAITurn++;
                    int selectedColumn = -1;

                    if (_currentAICoin.velocity.magnitude >= 0 && _currentAICoin.velocity.magnitude <= 1.5f)
                    {

                        if (_counterForAITurn == 1)
                        {
                            selectedColumn = AIMove();

                            Debug.Log("AI played column: " + selectedColumn);

                            for (int i = 0; i < 6; i++)
                            {
                                for (int j = 0; j < 7; j++)
                                {
                                    Debug.Log($"{i}, {j}: " + _boardState[i, j]);
                                }
                            }
                        }

                        PutAICoin(selectedColumn);

                        //var winner = CheckForWin();
                        //if (winner != 0)
                        //{
                        //    if (winner == 1)
                        //        SwitchState(State.PLAYERWIN);
                        //    else
                        //        SwitchState(State.AIWIN);
                        //}
                        //else if (RotateCameraAndLight1())
                        //{
                        //    SwitchState(State.PLAYER1TURN);
                        //    EnablePlayer1();
                        //    _counterForAITurn = 0;
                        //}
                    }
                }
                break;
        }
    }

    int AIMove()
    {
        _currentAICoin.isKinematic = true;

        Random rand = new Random();
        int randColumn;
        int validColumn;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (UpdateBoardState(j) == -1)
                    break;
                var winner = CheckForWin();

                if (winner == 2)
                {
                    RemoveCoinAt(j);

                    return j;
                }
                else
                {
                    RemoveCoinAt(j);
                }

                SwitchState(State.PLAYER1TURN);
                UpdateBoardState(j);
                winner = CheckForWin();

                if (winner == 1)
                {
                    RemoveCoinAt(j);
                    SwitchState(State.PLAYER2TURN);
                    UpdateBoardState(j);
                    RemoveCoinAt(j);

                    return j;
                }
                else
                {
                    RemoveCoinAt(j);
                }
            }
        }

        SwitchState(State.PLAYER2TURN);
        do
        {
            randColumn = rand.Next(0, 7);
            validColumn = UpdateBoardState(randColumn);
            //RemoveCoinAt(randColumn);
        } while (validColumn == -1);

        return randColumn;
    }

    void PutAICoin(int column)
    {
        player2Coins[0].gameObject.transform.rotation = Quaternion.Euler(90, player2Coins[0].rotation.y, 0);
        Debug.Log("0");
        if (_isCurrentPlayerEnabled && !IsColumnFull(column))
        {
            float _snapDuration = 0.4f;
            _elapsedTime += Time.deltaTime;
            _percentageComplete = _elapsedTime / _snapDuration;
            if (_percentageComplete >= 0 && _percentageComplete <= 1f)
            {
                if (column == 0)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0, _percentageComplete);
                else if (column == 1)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + offset, _percentageComplete);
                else if (column == 2)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + 2 * offset, _percentageComplete);
                else if (column == 3)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + 3 * offset, _percentageComplete);
                else if (column == 4)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + 4 * offset, _percentageComplete);
                else if (column == 5)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + 5 * offset, _percentageComplete);
                else if (column == 6)
                    player2Coins[0].gameObject.transform.position = Vector3.Lerp(player2Coins[0].startPosition, endPosition0 + 6 * offset, _percentageComplete);

                if (_percentageComplete >= 0.91f)
                {
                    _currentAICoin.isKinematic = false;
                    player2Coins[0].gameObject.transform.position = new Vector3(player2Coins[0].gameObject.transform.position.x, player2Coins[0].gameObject.transform.position.y, 0);
                    _isCurrentPlayerEnabled = false;
                }

            }
        }
        else
            _elapsedTime = 0;
    }

    void RemoveCoinAt(int column)
    {
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] != 0 && _boardState[i + 1, j] == 0)
                {
                    _boardState[i, j] = 0;
                    //Debug.Log($"Coin removed at {i}, {j}");
                }
            }
        }
    }

    public void DisableCurrentPlayer()
    {
        _isCurrentPlayerEnabled = false;
    }

    public void SetCurrentCoin(PickUpAndPlace coin)
    {
        _currentCoin = coin;
    }

    bool RotateCameraAndLight1()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= 178 && mainCamera.transform.rotation.eulerAngles.y <= 190)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);
            mainLight.transform.rotation = Quaternion.Euler(120, 20, 0);

            _isCurrentPlayerEnabled = true;

            return true;
        }

        return false;
    }

    bool RotateCameraAndLight2()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(-30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= -2 && mainCamera.transform.rotation.eulerAngles.y <= 6)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
            mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);

            _isCurrentPlayerEnabled = true;

            return true;
        }

        return false;
    }

    #region Enabling and Disabling players
    void EnablePlayer1()
    {
        //Debug.Log("Enabled player 1");

        foreach (var coin in player1Coins)
        {
            coin.enabled = true;
            coin.isCompletelyEnabled = true;
            coin.EnableCoinRotation();
        }
    }

    void EnablePlayer2()
    {
        //Debug.Log("Enabled player 2");

        foreach (var coin in player2Coins)
        {
            coin.enabled = true;
            coin.isCompletelyEnabled = true;
            coin.EnableCoinRotation();
        }
    }

    void DisablePlayer1()
    {
        //Debug.Log("Disabled player 1");

        foreach (var coin in player1Coins)
        {
            coin.enabled = false;
            coin.isCompletelyEnabled = false;
            coin.DisableCoinRotation();
        }
    }

    void DisablePlayer2()
    {
        //Debug.Log("Disabled player 2");

        foreach (var coin in player2Coins)
        {
            coin.enabled = false;
            coin.isCompletelyEnabled = false;
            coin.DisableCoinRotation();
        }
    }
    #endregion

    public int UpdateBoardState(int column)
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] == 0)
                {
                    if (state == State.PLAYER1TURN)
                    {
                        _boardState[i, j] = 1;
                        //Debug.Log($"Update put 1 in {i}, {j}");
                        return 1;
                    }
                    else if (state == State.PLAYER2TURN)
                    {
                        _boardState[i, j] = 2;
                        //Debug.Log($"Update put 2 in {i}, {j}");
                        return 2;
                    }

                    //Debug.Log("coin put " + i + ", " + j);
                }
            }
        }

        return -1;
    }

    public void OnePlayerClicked()
    {
        SwitchState(State.ONEPLAYER);
    }

    public void TwoPlayerClicked()
    {
        SwitchState(State.TWOPLAYERS);
    }

    #region WinCheck
    int CheckForWin()
    {
        if (CheckRows() != 0)
        {
            Debug.Log("Winner rows " + CheckRows());
            return CheckRows();
        }

        if (CheckColumns() != 0)
        {
            Debug.Log("Winner columns " + CheckColumns());
            return CheckColumns();
        }

        if (CheckMinorDiagonals() != 0)
        {
            Debug.Log("Winner minor diagonal " + CheckMinorDiagonals());
            return CheckMinorDiagonals();
        }

        if (CheckMajorDiagonals() != 0)
        {
            Debug.Log("Winner major diagonal " + CheckMajorDiagonals());
            return CheckMajorDiagonals();
        }

        if (IsBoardFull())
        {
            Debug.Log("Board is full!");
            SwitchState(State.DRAW);
        }

        return 0;
    }

    int CheckForAWinner(int[] coins)
    {
        int countA = 0;
        int countB = 0;

        for (int i = 0; i < coins.Length; i++)
        {
            if (coins[i] == 0)
            {
                countA = 0;
                countB = 0;
            }
            else if (coins[i] == 1)
            {
                countA++;
                countB = 0;
            }
            else
            {
                countB++;
                countA = 0;
            }

            if (countA >= 4)
                return 1;
            else if (countB >= 4)
                return 2;
        }

        return 0;
    }

    int CheckRows()
    {
        int[] coins = new int[7];

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    coins[j] = _boardState[i, j];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckColumns()
    {
        int[] coins = new int[6];

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (_boardState[j, i] != 0)
                    coins[j] = _boardState[j, i];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMinorDiagonals()
    {
        int[] coins = new int[6];

        int sum = 3;
        int newIndex = 0;
        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i + j == sum)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            sum++;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMajorDiagonals()
    {
        int[] coins = new int[6];
        int difference = 2;
        int newIndex = 0;

        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 5; i >= 0; i--)
            {
                for (int j = 6; j >= 0; j--)
                {
                    if (i - j == difference)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            difference--;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }
    #endregion

    public bool IsColumnFull(int column)
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 6;
    }

    bool IsBoardFull()
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 42;
    }

    public void GoToMenu()
    {
        SwitchState(State.INIT);
    }
}
