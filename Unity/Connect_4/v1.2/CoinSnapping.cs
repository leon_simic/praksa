using UnityEngine;

public class CoinSnapping : MonoBehaviour
{
    public Collider[] columnColliders;

    bool isMouseUp;
    float snapDuration = 0.1f;
    float elapsedTime;
    float percentageComplete;
    Vector3 startPosition;

    void Update()
    {
        RaycastHit hit;
        Ray coinRay = new Ray(new Vector3(transform.position.x, transform.position.y, -transform.position.z - 2 * Mathf.Abs(transform.position.z)), Vector3.forward * 30);

        //Gizmo
        Debug.DrawRay(new Vector3(transform.position.x, transform.position.y, -transform.position.z - 2 * Mathf.Abs(transform.position.z)), Vector3.forward * 30);

        if (Physics.Raycast(coinRay, out hit, 100))
        {
            if (isMouseUp)
            {
                if (hit.collider == columnColliders[0] || hit.collider == columnColliders[1] ||
                    hit.collider == columnColliders[2] || hit.collider == columnColliders[3] ||
                    hit.collider == columnColliders[4] || hit.collider == columnColliders[5] ||
                    hit.collider == columnColliders[6])
                {
                    elapsedTime += Time.deltaTime;
                    percentageComplete = elapsedTime / snapDuration;

                    transform.position = Vector3.Lerp(startPosition, hit.transform.position, percentageComplete);
                    if (percentageComplete >= 0.8)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                    }
                } 
            }
        }
    }

    void OnMouseUp()
    {
        isMouseUp = true;

        startPosition = transform.position;
    }
    void OnMouseDown()
    {
        isMouseUp = false;
    }
}
