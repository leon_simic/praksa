using UnityEngine;

public class CoinRotate : MonoBehaviour
{
    Rigidbody rb;

    Vector3 angleVelocity = new Vector3(180, 0, 0);
    bool rotateCoin;
    bool hasCollided;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void OnMouseDown()
    {
        rotateCoin = true;
    }

    void FixedUpdate()
    {
        Quaternion deltaRotation = Quaternion.Euler(angleVelocity * Time.fixedDeltaTime);

        if (rotateCoin && rb.rotation.eulerAngles.x < 89 && !hasCollided)
        {
            rb.MoveRotation(rb.rotation * deltaRotation);

            if (rb.rotation.eulerAngles.x > 89)
            {
                rb.rotation = Quaternion.Euler(90, 180, 0);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        hasCollided = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        hasCollided = false;
    }

    void OnMouseUp()
    {
        rb.isKinematic = true;
        rotateCoin = false;
        rb.isKinematic = false;
    }
}
