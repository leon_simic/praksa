using System;
using System.Net.Http.Headers;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public Text connect4Text;

    public GameObject menuPanel;
    public GameObject player1WinPanel;
    public GameObject player2WinPanel;
    public GameObject playerWinPanel;
    public GameObject aiWinPanel;
    public GameObject playAgainPanel;

    public GameObject[] player1Coins;
    public GameObject[] player2Coins;
    public bool player1Turn = true;
    public GameObject mainCamera;
    public Light mainLight;

    int[,] _boardState;
    GameObject _currentCoin;
    Rigidbody _currentCoinRb;
    public bool _isCurrentPlayerEnabled;

    public enum State { MENU, ONEPLAYER, TWOPLAYERS, PLAYER1WIN, PLAYER2WIN, PLAYERWIN, AIWIN }
    State _state;

    public void OnePlayerClicked()
    {
        SwitchState(State.ONEPLAYER);
    }

    public void TwoPlayerClicked()
    {
        SwitchState(State.TWOPLAYERS);
    }

    void Start()
    {
        SwitchState(State.MENU);
        _boardState = new int[6, 7];
        _isCurrentPlayerEnabled = true;
    }

    void SwitchState(State newState)
    {
        EndState();
        BeginState(newState);
    }

    void BeginState(State newState)
    {
        switch (newState)
        {
            case State.MENU:
                menuPanel.SetActive(true);
                break;
            case State.ONEPLAYER:
                break;
            case State.TWOPLAYERS:
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(true);
                playAgainPanel.SetActive(true);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(true);
                playAgainPanel.SetActive(true);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(true);
                playAgainPanel.SetActive(true);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(true);
                playAgainPanel.SetActive(true);
                break;
        }
    }

    void EndState()
    {
        switch (_state)
        {
            case State.MENU:
                menuPanel.SetActive(false);
                break;
            case State.ONEPLAYER:
                break;
            case State.TWOPLAYERS:
                break;
            case State.PLAYER1WIN:
                player1WinPanel.SetActive(false);
                playAgainPanel.SetActive(false);
                break;
            case State.PLAYER2WIN:
                player2WinPanel.SetActive(false);
                playAgainPanel.SetActive(false);
                break;
            case State.PLAYERWIN:
                playerWinPanel.SetActive(false);
                playAgainPanel.SetActive(false);
                break;
            case State.AIWIN:
                aiWinPanel.SetActive(false);
                playAgainPanel.SetActive(false);
                break;
        }
    }

    void Update()
    {
        switch (_state)
        {
            case State.MENU:
                break;
            case State.ONEPLAYER:
                break;
            case State.TWOPLAYERS:
                break;
            case State.PLAYER1WIN:
                break;
            case State.PLAYER2WIN:
                break;
            case State.PLAYERWIN:
                break;
            case State.AIWIN:
                break;
        }


        if (player1Turn)
        {
            if (!_isCurrentPlayerEnabled)
            {
                Debug.Log("disable player1");
                DisablePlayer1();
            }
            else
            {
                Debug.Log("enable player1");
                EnablePlayer1();
            }
        }
        else
        {
            if (!_isCurrentPlayerEnabled)
                DisablePlayer2();
            else
                EnablePlayer2();
        }

        if (_currentCoin != null)
            _currentCoinRb = _currentCoin.GetComponent<Rigidbody>();

        if (_currentCoin != null && _currentCoin.transform.position.y < 8.7 && _currentCoinRb.velocity.magnitude == 0)
        {
            var winner = CheckForWin();
            if (winner != 0)
            {
                if (winner == 1)
                    SwitchState(State.PLAYER1WIN);
                else
                    SwitchState(State.PLAYER2WIN);
            }
                

            if (player1Turn)
            {
                RotateCameraAndLight2();
            }
            else if (!player1Turn)
            {
                RotateCameraAndLight1();
            }
        }
    }

    public void DisableCurrentPlayer()
    {
        _isCurrentPlayerEnabled = false;
    }

    public bool TakeTurn(int column)
    {
        var isFull = !UpdateBoardState(column);
        //Debug.Log("Call UpdateBoard");

        if (!isFull)
        {

            if (player1Turn)
            {
                player1Turn = false;
            }
            else
            {
                player1Turn = true;
            }
        }

        return !isFull;
    }

    public void SetCurrentCoin(GameObject coin)
    {
        _currentCoin = coin;
    }


    void RotateCameraAndLight1()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= 180 && mainCamera.transform.rotation.eulerAngles.y <= 190)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 180, 0);
            mainLight.transform.rotation = Quaternion.Euler(120, 20, 0);

            _isCurrentPlayerEnabled = true;
        }
    }

    void RotateCameraAndLight2()
    {
        mainCamera.transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime * 3f);
        mainLight.transform.Rotate(new Vector3(-30, 0, 0) * Time.deltaTime * 3f);

        if (mainCamera.transform.rotation.eulerAngles.y >= 0 && mainCamera.transform.rotation.eulerAngles.y <= 6)
        {
            mainCamera.transform.rotation = Quaternion.Euler(0, 0, 0);
            mainLight.transform.rotation = Quaternion.Euler(60, 20, 0);

            _isCurrentPlayerEnabled = true;
        }
    }

    #region Enabling and Disabling players
    void EnablePlayer1()
    {
        foreach (var coin in player1Coins)
        {
            coin.GetComponent<PickUpAndPlace>().isEnabled = true;
            coin.GetComponent<CoinRotate>().enabled = true;

        }
    }

    void EnablePlayer2()
    {
        foreach (var coin in player2Coins)
        {
            coin.GetComponent<PickUpAndPlace>().isEnabled = true;
            coin.GetComponent<CoinRotate>().enabled = true;

        }
    }

    void DisablePlayer1()
    {
        foreach (var coin in player1Coins)
        {
            coin.GetComponent<PickUpAndPlace>().isEnabled = false;
            coin.GetComponent<CoinRotate>().enabled = false;

        }
    }

    void DisablePlayer2()
    {
        foreach (var coin in player2Coins)
        {
            coin.GetComponent<PickUpAndPlace>().isEnabled = false;
            coin.GetComponent<CoinRotate>().enabled = false;

        }
    }
    #endregion

    public bool UpdateBoardState(int column)
    {
        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] == 0)
                {
                    if (player1Turn)
                    {
                        //Debug.Log("player1turn");
                        _boardState[i, j] = 1;
                    }
                    else if (!player1Turn)
                    {
                        //Debug.Log("player2turn");
                        _boardState[i, j] = 2;
                    }

                    Debug.Log("coin put " + i + ", " + j);
                    return true;
                }
            }
        }

        return false;
    }

    int CheckForWin()
    {
        if (CheckRows() != 0)
        {
            Debug.Log("Winner rows " + CheckRows());
            return CheckRows();
        }

        if (CheckColumns() != 0)
        {
            Debug.Log("Winner columns " + CheckColumns());
            return CheckColumns();
        }

        if (CheckMinorDiagonals() != 0)
        {
            Debug.Log("Winner minor diagonal " + CheckMinorDiagonals());
            return CheckMinorDiagonals();
        }

        if (CheckMajorDiagonals() != 0)
        {
            Debug.Log("Winner major diagonal " + CheckMajorDiagonals());
            return CheckMajorDiagonals();
        }

        if (IsBoardFull())
        {
            Debug.Log("Board is full!");
        }

        return 0;
    }

    int CheckForAWinner(int[] coins)
    {
        int countA = 0;
        int countB = 0;

        for (int i = 0; i < coins.Length; i++)
        {
            if (coins[i] == 0)
            {
                countA = 0;
                countB = 0;
            }
            else if (coins[i] == 1)
            {
                countA++;
                countB = 0;
            }
            else
            {
                countB++;
                countA = 0;
            }

            if (countA >= 4)
                return 1;
            else if (countB >= 4)
                return 2;
        }

        return 0;
    }

    int CheckRows()
    {
        int[] coins = new int[7];

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    coins[j] = _boardState[i, j];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckColumns()
    {
        int[] coins = new int[6];

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                if (_boardState[j, i] != 0)
                    coins[j] = _boardState[j, i];
            }
            var result = CheckForAWinner(coins);

            if (result > 0)
                return result;

            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMinorDiagonals()
    {
        int[] coins = new int[6];

        int sum = 3;
        int newIndex = 0;
        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (i + j == sum)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            sum++;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    int CheckMajorDiagonals()
    {
        int[] coins = new int[6];
        int difference = 2;
        int newIndex = 0;

        for (int diagonal = 0; diagonal < 6; diagonal++)
        {
            for (int i = 5; i >= 0; i--)
            {
                for (int j = 6; j >= 0; j--)
                {
                    if (i - j == difference)
                    {
                        coins[newIndex] = _boardState[i, j];
                        newIndex++;
                    }

                    var result = CheckForAWinner(coins);

                    if (result > 0)
                        return result;
                }
            }
            difference--;
            newIndex = 0;
            Array.Clear(coins, 0, coins.Length);
        }

        return 0;
    }

    public bool IsColumnFull(int column)
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (j == column && _boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 6;
    }

    bool IsBoardFull()
    {
        int count = 0;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (_boardState[i, j] != 0)
                    count++;
            }
        }

        return count == 42;
    }
}
