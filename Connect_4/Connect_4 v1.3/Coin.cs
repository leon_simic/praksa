namespace Connect_4
{
    enum PlayerType
    {
        PlayerA,
        PlayerB
    }

    internal class Coin
    {
        public PlayerType Type { get; private set; }

        public Coin(PlayerType type)
        {
            Type = type;
        }
    }
}
