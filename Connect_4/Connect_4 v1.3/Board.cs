using System;

namespace Connect_4
{
    internal class Board
    {
        public const ConsoleColor PLAYER_A_COLOR = ConsoleColor.Red;
        public const ConsoleColor PLAYER_B_COLOR = ConsoleColor.Blue;

        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Field[,] Fields { get; private set; }

        public Board()
        {
            Rows = 6 + 1;
            Columns = 7;
            Fields = new Field[Rows, Columns];
            Generate();
        }

        private void Generate()
        {
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Fields[i, j] = new Field();
                }
            }
        }

        public int InsertCoinAt(Coin coin, int selectedColumn)
        {
            selectedColumn--;
            for (int i = Rows - 2; i >= 0; i--)
            {
                for (int j = Columns - 1; j >= 0; j--)
                {
                    if (j == selectedColumn && Fields[i, j].Coin is null)
                    {
                        Fields[i, j].Populate(coin);
                        return 0;
                    }
                    else if (j == selectedColumn && i == 0)
                        return 1;
                }
            }
            return 0;
        }

        public Coin CheckForWin()
        {
            return CheckForWinVertically();
            //int counter1 = 1;
            //int counter2 = 1;
            //Disc winningPlayer = null;

            //for (int i = 0; i < Rows - 1; i++)
            //{
            //    for (int j = 0; j < Columns - 1; j++)
            //    {
            //        if (Holder[i, j].Letter == Players[0].Letter)
            //        {
            //            //Prva boja
            //            if (Holder[i, j].Color == Players[0].Color)
            //            {
            //                if (Holder[i, j].Color == Holder[i + 1, j].Color)
            //                {
            //                    counter1++;
            //                    Console.WriteLine("c1: " + counter1);
            //                    Console.WriteLine("c2: " + counter2);
            //                    winningPlayer = Holder[i, j];
            //                }
            //            }
            //            else if (Holder[i, j].Color == Players[1].Color)
            //            {
            //                if (Holder[i, j].Color == Holder[i + 1, j].Color)
            //                {
            //                    counter2++;
            //                    Console.WriteLine("c1: " + counter1);
            //                    Console.WriteLine("c2: " + counter2);
            //                    winningPlayer = Holder[i, j];
            //                }
            //            }
            //        }
            //        if (counter1 == 4 || counter2 == 4)
            //        {
            //            return winningPlayer;
            //        }
            //    }
            //}
            //return null;
        }

        private Coin CheckForWinVertically()
        {
            Coin winningPlayer = null;

            foreach (var player in Enum.GetValues(typeof(PlayerType)))
            {
                //int counter = 1;
                for (int i = 0; i < Rows - 2; i++)
                {
                    for (int j = 0; j < Columns - 1; j++)
                    {
                        if (Fields[i, j] is object && Fields[i + 1, j] is object)
                        {
                            if (Fields[i, j].Coin == Fields[i + 1, j].Coin)
                            {
                                Console.WriteLine("coin je " + Fields[i, j].Coin.ToString());
                                //counter++;
                                //Console.WriteLine("counter is " + counter);
                                winningPlayer = Fields[i, j].Coin;
                            }
                        }
                    }
                }
            }
            return winningPlayer;
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    //Zadnji red je za brojeve
                    if (i == Rows - 1)
                        PrintColumns(j);
                    else
                    {
                        if (Fields[i, j].IsPopulated())
                        {
                            var coinColor = Fields[i, j].Coin.Type == PlayerType.PlayerA ? PLAYER_A_COLOR : PLAYER_B_COLOR;
                            PrintPopulated(coinColor);
                        }
                        else
                            PrintEmpty();
                    }
                }
                Console.WriteLine();
            }
        }

        private void PrintColumns(int counter)
        {
            Console.Write(counter + 1 + " ");
        }

        private void PrintPopulated(ConsoleColor coinColor)
        {
            Console.ForegroundColor = coinColor;
            Console.Write("O ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void PrintEmpty()
        {
            Console.Write("_ ");            
        }
    }
}