using System;

namespace Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Board board = new Board();
            Coin[] coins = new Coin[]
            {
                new Coin(PlayerType.PlayerA),
                new Coin(PlayerType.PlayerB)
            };

            board.Print();

            int selectedColumn;
            do
            {
                int insertCheck;
                int coinIndex = 0;

                foreach (var player in Enum.GetNames(typeof(PlayerType)))
                {
                    do
                    {
                        Console.WriteLine();
                        Console.WriteLine($"{player} is on turn.");
                        Console.WriteLine("Enter column number: ");
                        selectedColumn = int.Parse(Console.ReadLine());

                        insertCheck = board.InsertCoinAt(coins[coinIndex], selectedColumn);

                        if (insertCheck != 0)
                            Console.WriteLine("The selected column is full. ");

                        board.Print();

                        //if (board.CheckForWin() != null)
                        //{
                        //    Console.WriteLine("Winner is: " + player);
                        //    return;
                        //}
                    } while (selectedColumn <= 0 || selectedColumn > board.Columns || insertCheck != 0);

                    coinIndex++;
                }
            } while (true);
        }
    }
}
