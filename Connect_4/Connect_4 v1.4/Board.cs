using System;

namespace Connect_4
{
    internal class Board
    {
        public const ConsoleColor PLAYER_A_COLOR = ConsoleColor.Red;
        public const ConsoleColor PLAYER_B_COLOR = ConsoleColor.Blue;

        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Field[,] Fields { get; private set; }

        public Board()
        {
            Rows = 6 + 1;
            Columns = 7;
            Fields = new Field[Rows, Columns];
            Generate();
        }

        private void Generate()
        {
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Fields[i, j] = new Field();
                }
            }
        }

        public int InsertCoinAt(Coin coin, int selectedColumn)
        {
            selectedColumn--;
            for (int i = Rows - 2; i >= 0; i--)
            {
                for (int j = Columns - 1; j >= 0; j--)
                {
                    if (j == selectedColumn && Fields[i, j].Coin is null)
                    {
                        Fields[i, j].Populate(coin);
                        return 0;
                    }
                    else if (j == selectedColumn && i == 0)
                        return 1;
                }
            }
            return 0;
        }

        private int CheckForAWinner(Coin[] coins)
        {
            int countA = 0;
            int countB = 0;

            for (int i = 0; i < coins.Length; i++)
            {
                if (coins[i] == null)
                {
                    countA = 0;
                    countB = 0;
                }
                else if (coins[i].Type == PlayerType.PlayerA)
                {
                    countA++;
                    countB = 0;
                }
                else
                {
                    countB++;
                    countA = 0;
                }

                if (countA >= 4)
                    return 1;
                else if (countB >= 4)
                    return 2;
            }

            return 0;
        }

        public int CheckColumns()
        {
            Coin[] coins = new Coin[Rows];

            for (int i = 0; i < Columns; i++)
            {
                for (int j = 0; j < Rows; j++) 
                    coins[j] = Fields[i, j].Coin; 

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;
            }

            return 0;
        }

        public int CheckRows()
        {
            Coin[] coins = new Coin[Columns];

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                    coins[j] = Fields[i, j].Coin;

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;
            }

            return 0;
        }

        public int CheckMainDiagonals()
        {
            //int[] numbers = new int[100];
            //int[,] matrix = new int[,]
            //{
            //    {3, 4, 1, 9 },
            //    {2, 42, 12, 38 },
            //    {12, 51, 22, 11 }
            //};
            //int newIndex1 = 0;
            
            //int difference1 = -1;
            //for (int k = 0; k < 4; k++)
            //{
            //    for (int i = 2; i >= 0; i--)
            //    {
            //        for (int j = 3; j >= 0; j--)
            //        {
            //            if (j - i == difference1)
            //            {
            //                numbers[newIndex1] = matrix[i, j];
            //                Console.WriteLine(numbers[newIndex1]);
            //                difference1 = j - i;
            //                newIndex1++;
            //                continue;
            //            }
            //        }
            //    }
            //    difference1 += 1;
            //    Console.WriteLine();
            //}


            Coin[] coins = new Coin[Columns];
            int newIndex = 0;
            int difference = Rows - Columns;

            for (int k = 0; k < 6; k++)
            {
                for (int i = Rows - 2; i >= 0; i--)
                {
                    for (int j = Columns - 1; j >= 0; j--)
                    {
                        if (j - i == difference)
                        {
                            coins[newIndex] = Fields[i, j].Coin;
                            difference = j - i;
                            newIndex++;
                            continue;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                difference += 1;
            }

            return 0;
        }


        //public Coin CheckForWin()
        //{
        //    return CheckForWinVertically();
        //}

        //private Coin CheckForWinVertically()
        //{
        //    Coin winningPlayer = null;

        //    foreach (var player in Enum.GetValues(typeof(PlayerType)))
        //    {
        //        int counter = 1;
        //        for (int i = 0; i < Rows - 2; i++)
        //        {
        //            for (int j = 0; j < Columns; j++)
        //            {
        //                if (Fields[i, j].IsPopulated() && Fields[i + 1, j].IsPopulated())
        //                {
        //                    if (Fields[i, j].Coin.Type == Fields[i + 1, j].Coin.Type)
        //                    {
        //                        counter++;
        //                        Console.WriteLine("counter is " + counter);
        //                        winningPlayer = Fields[i, j].Coin;
        //                    }

        //                    if (counter == 4)
        //                        return winningPlayer;
        //                }
        //            }
        //            counter = 1;
        //        }
        //    }
        //    return null;
        //}

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    //Zadnji red je za brojeve
                    if (i == Rows - 1)
                        PrintColumns(j);
                    else
                    {
                        if (Fields[i, j].IsPopulated())
                        {
                            var coinColor = Fields[i, j].Coin.Type == PlayerType.PlayerA ? PLAYER_A_COLOR : PLAYER_B_COLOR;
                            PrintPopulated(coinColor);
                        }
                        else
                            PrintEmpty();
                    }
                }
                Console.WriteLine();
            }
        }

        private void PrintColumns(int counter)
        {
            Console.Write(counter + 1 + " ");
        }

        private void PrintPopulated(ConsoleColor coinColor)
        {
            Console.ForegroundColor = coinColor;
            Console.Write("O ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void PrintEmpty()
        {
            Console.Write("_ ");            
        }
    }
}