using System;

namespace Connect_4
{
    internal class Board
    {
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Field[,] Fields { get; private set; }
        public Coin[] Players { get; private set; }

        public Board()
        {
            Rows = 7;
            Columns = 7;
            Fields = new Field[Rows, Columns];
            Players = new Coin[]
            {
                new Coin(ConsoleColor.Red, "O "),
                new Coin(ConsoleColor.Blue, "O "),
            };
            Generate();
        }

        private void Generate()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    //U zadnji redak idu brojevi
                    if (i == Rows - 1)
                    {
                        Fields[i, j] = new Field();
                        Fields[i, j].Populate(new Coin(ConsoleColor.White, (j + 1) + " "));
                    }
                    else
                        Fields[i, j] = new Field();
                }
            }
        }

        public int InsertCoinAt(Coin coin, int selectedColumn)
        {
            selectedColumn--;
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (j == selectedColumn && Fields[i + 1, j].Coin.Letter != Fields[i, j].Coin.Letter)
                    {
                        Fields[i, j].Populate(coin);
                        return 0;
                    }
                    else if (j == selectedColumn && Fields[i + 1, j].Coin.Letter == Fields[i, j].Coin.Letter && Fields[i, j].Coin.Letter != Fields[i, j].Blank.Letter)
                        return 1;
                }
            }
            return 0;
        }

        public Coin CheckForWin()
        {
            return CheckForWinVertically();
            //int counter1 = 1;
            //int counter2 = 1;
            //Disc winningPlayer = null;

            //for (int i = 0; i < Rows - 1; i++)
            //{
            //    for (int j = 0; j < Columns - 1; j++)
            //    {
            //        if (Holder[i, j].Letter == Players[0].Letter)
            //        {
            //            //Prva boja
            //            if (Holder[i, j].Color == Players[0].Color)
            //            {
            //                if (Holder[i, j].Color == Holder[i + 1, j].Color)
            //                {
            //                    counter1++;
            //                    Console.WriteLine("c1: " + counter1);
            //                    Console.WriteLine("c2: " + counter2);
            //                    winningPlayer = Holder[i, j];
            //                }
            //            }
            //            else if (Holder[i, j].Color == Players[1].Color)
            //            {
            //                if (Holder[i, j].Color == Holder[i + 1, j].Color)
            //                {
            //                    counter2++;
            //                    Console.WriteLine("c1: " + counter1);
            //                    Console.WriteLine("c2: " + counter2);
            //                    winningPlayer = Holder[i, j];
            //                }
            //            }
            //        }
            //        if (counter1 == 4 || counter2 == 4)
            //        {
            //            return winningPlayer;
            //        }
            //    }
            //}
            //return null;
        }

        private Coin CheckForWinVertically()
        {
            //Disc winningPlayer = null;

            //for (int i = 0; i < Rows - 4; i++)
            //{
            //    for (int j = 0; j < Columns; j++)
            //    {
            //        if (Holder[i, j].Letter == Players[0].Letter)
            //        {
            //            //Prva boja
            //            if (Holder[i, j].Color == Players[0].Color)
            //            {
            //                if (Holder[i, j].Color == Holder[i + 1, j].Color)
            //                {

            //                    winningPlayer = Holder[i, j];
            //                }
            //            }
            //        }
            //    }
            //}

            return null;
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Console.ForegroundColor = Fields[i, j].Coin.Color;
                    Console.Write(Fields[i, j].ToString());
                }
                Console.WriteLine();
            }
        }
    }
}