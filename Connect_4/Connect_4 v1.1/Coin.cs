using System;

namespace Connect_4
{
    enum PlayerType
    {
        PlayerRed,
        PlayerBlue 
    }

    internal class Coin
    {
        public PlayerType Type { get; private set; }
        public ConsoleColor Color { get; private set; }
        public string Letter { get; private set; }

        public Coin(ConsoleColor color, string letter)
        {
            Color = color;
            Letter = letter;
        }
    }
}
