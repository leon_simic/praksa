using System;

namespace Connect_4
{
    internal class Field
    {
        public Coin Coin { get; private set; }
        public Coin Blank { get; private set; }
        
        public Field()
        {
            Blank = new Coin(ConsoleColor.White, "_ ");
            Coin = Blank;
        }

        public void Populate(Coin coin)
        {
            Coin = coin;
        }

        public bool IsPopulated()
        {
            if (Coin == Blank)
                return false;
            return true;
        }

        public override string ToString()
        {
            return Coin.Letter;
        }
    }
}
