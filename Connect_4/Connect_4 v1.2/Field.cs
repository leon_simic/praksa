using System;

namespace Connect_4
{
    internal class Field
    {
        public Coin Coin { get; private set; }

        public Field()
        {
            Coin = null;
        }
        public void Populate(Coin coin)
        {
            Coin = coin;
        }

        public bool IsPopulated()
        {
            return Coin != null;
        }

        public void Clear()
        {
            Coin = null;
        }
    }
}
