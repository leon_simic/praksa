using System;

namespace Connect_4
{
    internal class Board
    {
        public const ConsoleColor PLAYER_A_COLOR = ConsoleColor.Red;
        public const ConsoleColor PLAYER_B_COLOR = ConsoleColor.Blue;

        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Field[,] Fields { get; private set; }

        public Board()
        {
            Rows = 6 + 1;
            Columns = 7;
            Fields = new Field[Rows, Columns];
            Generate();
        }

        private void Generate()
        {
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Fields[i, j] = new Field();
                }
            }
        }

        public bool IsFull()
        {
            int count = 0;

            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Fields[i, j].IsPopulated())
                        count++;
                }
            }

            return count == (Rows - 1) * Columns;
        }

        public void Clear()
        {
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Fields[i, j].Clear();
                }
            }
        }

        public bool InsertCoinAt(Coin coin, int selectedColumn)
        {
            selectedColumn--;
            for (int i = Rows - 2; i >= 0; i--)
            {
                for (int j = Columns - 1; j >= 0; j--)
                {
                    if (j == selectedColumn && Fields[i, j].Coin is null)
                    {
                        Fields[i, j].Populate(coin);
                        return true;
                    }
                    else if (j == selectedColumn && i == 0)
                        return false;
                }
            }
            return false;
        }

        public bool RemoveCoinAt(int selectedColumn)
        {
            selectedColumn--;
            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (j == selectedColumn && Fields[i, j].Coin != null)
                    {
                        Fields[i, j].Clear();
                        return true;
                    }
                    else if (j == selectedColumn && i == Rows - 2)
                        return false;
                }
            }
            return false;
        }

        public void PlayAI(Coin[] coins)
        {
            Random rand = new Random();
            int randColumn;

            for (int i = Rows - 2; i >= 0; i--)
            {
                for (int j = Columns; j >= 0; j--)
                {
                    if (InsertCoinAt(coins[1], j))
                        if (CheckForWin() == 2)
                        {
                            return;
                        }
                        else
                        {
                            RemoveCoinAt(j);
                        }

                    if (InsertCoinAt(coins[0], j))
                    {
                        if (CheckForWin() == 1)
                        {
                            RemoveCoinAt(j);
                            InsertCoinAt(coins[1], j);
                            return;
                        }
                        else
                        {
                            RemoveCoinAt(j);
                        }
                    }   
                }
            }

            randColumn = rand.Next(1, 8);
            InsertCoinAt(coins[1], randColumn);
        }

        public int CheckForWin()
        {
            if (CheckRows() != 0)
                return CheckRows();


            if (CheckColumns() != 0)
                return CheckColumns();


            if (CheckMajorDiagonals() != 0)
                return CheckMajorDiagonals();


            if (CheckMinorDiagonals() != 0)
                return CheckMinorDiagonals();

            return 0;
        }

        private int CheckForAWinner(Coin[] coins)
        {
            int countA = 0;
            int countB = 0;

            for (int i = 0; i < coins.Length; i++)
            {
                if (coins[i] == null)
                {
                    countA = 0;
                    countB = 0;
                }
                else if (coins[i].Type == PlayerType.PlayerA)
                {
                    countA++;
                    countB = 0;
                }
                else
                {
                    countB++;
                    countA = 0;
                }

                if (countA >= 4)
                    return 1;
                else if (countB >= 4)
                    return 2;
            }

            return 0;
        }

        private int CheckRows()
        {
            Coin[] coins = new Coin[Rows];

            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Fields[i, j].IsPopulated())
                        coins[j] = Fields[i, j].Coin;
                }
                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckColumns()
        {
            Coin[] coins = new Coin[Columns];

            for (int i = 0; i < Columns; i++)
            {
                for (int j = 0; j < Rows - 1; j++)
                {
                    if (Fields[j, i].IsPopulated())
                        coins[j] = Fields[j, i].Coin;
                }

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMinorDiagonals()
        {
            Coin[] coins = new Coin[Rows];

            int sum = 3;
            int newIndex = 0;
            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = 0; i < Rows - 1; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        if (i + j == sum)
                        {
                            coins[newIndex] = Fields[i, j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                sum++;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMajorDiagonals()
        {
            Coin[] coins = new Coin[Rows];
            int difference = 2;
            int newIndex = 0;

            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = Rows - 2; i >= 0; i--)
                {
                    for (int j = Columns - 1; j >= 0; j--)
                    {
                        if (i - j == difference)
                        {
                            coins[newIndex] = Fields[i, j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                difference--;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    //Zadnji red je za brojeve
                    if (i == Rows - 1)
                        PrintColumns(j);
                    else
                    {
                        if (Fields[i, j].IsPopulated())
                        {
                            var coinColor = Fields[i, j].Coin.Type == PlayerType.PlayerA ? PLAYER_A_COLOR : PLAYER_B_COLOR;
                            PrintPopulated(coinColor);
                        }
                        else
                            PrintEmpty();
                    }
                }
                Console.WriteLine();
            }
        }

        private void PrintColumns(int counter)
        {
            Console.Write(counter + 1 + " ");
        }

        private void PrintPopulated(ConsoleColor coinColor)
        {
            Console.ForegroundColor = coinColor;
            Console.Write("O ");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void PrintEmpty()
        {
            Console.Write("_ ");
        }
    }
}