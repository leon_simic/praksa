using System;
using System.Drawing;

namespace Connect_4
{
    internal class Disc
    {
        public ConsoleColor Color { get; set; }
        public string Letter { get; set; }

        public Disc(ConsoleColor color)
        {
            Color = color;
            Letter = "O ";
        }
    }
}
