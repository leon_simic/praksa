using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect_4
{
    internal class Field
    {
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Disc[,] Holder { get; private set; }
        public Disc[] Players { get; private set; }

        public Field()
        {
            Rows = 6 + 1;
            Columns = 7;

            Players = new Disc[]
            {
                new Disc(ConsoleColor.Red),
                new Disc(ConsoleColor.Blue)
            };
        }

        public Field(int rows, int columns)
        {
            Rows = rows + 1; 
            Columns = columns;

            Players = new Disc[]
            {
                new Disc(ConsoleColor.Red),
                new Disc(ConsoleColor.Blue)
            };
        }

        public void Generate()
        {
            Disc[,] field = new Disc[Rows, Columns];

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    field[i, j] = new Disc(ConsoleColor.White);
                }
            }

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (i == Rows - 1)
                        field[i, j].Letter = (j + 1) + " ";
                    else
                        field[i, j].Letter = "_ ";

                    if (j == Columns)
                        field[i, j].Letter = Environment.NewLine;
                }

            }

            Holder = field;
        }

        public void PutDiscAt(int indexOfPlayer, int selectedColumn)
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (j == selectedColumn && Holder[i, selectedColumn].Letter == "_ " && Holder[i + 1, selectedColumn].Letter != "_ ")
                    {
                        Console.ForegroundColor = Players[indexOfPlayer].Color;
                        Holder[i, selectedColumn].Letter = Players[indexOfPlayer].Letter;
                        Holder[i, selectedColumn].Color = Players[indexOfPlayer].Color;
                        Console.Write(Holder[i, selectedColumn].Letter);
                    }
                    else if (Holder[i, j].Letter == Players[indexOfPlayer].Letter)
                    {
                        Console.ForegroundColor = Holder[i, j].Color;
                        Console.Write(Holder[i, j].Letter);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.Write(Holder[i, j].Letter);
                    }
                }
                Console.WriteLine();
            }
        }

        public Disc CheckForWin()
        {
            int counter = 1;
            Disc winningPlayer = null;

            for (int i = 0; i < Rows - 1; i++)
            {
                for (int j = 0; j < Columns - 1; j++)
                {
                    if (Holder[i, j].Letter == Players[0].Letter && Holder[i, j].Color == Players[0].Color)
                        if (Holder[i, j].Color == Holder[i + 1, j].Color)
                        {
                            counter++;
                            Console.WriteLine(counter);
                            winningPlayer = Holder[i, j];
                        }
                    if (counter == 4)
                    {
                        return winningPlayer;
                    }
                }
            }
            return null;
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Console.Write(Holder[i, j].Letter);
                }
                Console.WriteLine();
            }
        }
    }
}
