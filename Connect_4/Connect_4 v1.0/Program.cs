using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {   
            Console.WriteLine("Enter number of rows: ");
            int rows = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter number of columns: ");
            int columns = int.Parse(Console.ReadLine());

            Field field = new Field(rows, columns);

            field.Generate();
            field.Print();
            bool isOver = false;

            Disc player1 = new Disc(ConsoleColor.Red);
            Disc player2 = new Disc(ConsoleColor.Blue);

            int selectedColumn;

            do
            {
                Console.WriteLine();
                Console.WriteLine("Red is on turn.");

                do
                {
                    Console.WriteLine("Enter column number: ");
                    selectedColumn = int.Parse(Console.ReadLine());
                } while (selectedColumn <= 0 || selectedColumn > field.Columns || string.IsNullOrEmpty(selectedColumn.ToString()));
                field.PutDiscAt(0, selectedColumn - 1);

                if (field.CheckForWin() != null)
                {
                    Console.WriteLine();
                    Console.WriteLine("Winner is: " + field.CheckForWin().Color);
                    return;
                }

                Console.WriteLine();
                Console.WriteLine("Blue is on turn.");

                do
                {
                    Console.WriteLine("Enter column number: ");
                    selectedColumn = int.Parse(Console.ReadLine());
                } while (selectedColumn <= 0 || selectedColumn > field.Columns || string.IsNullOrEmpty(selectedColumn.ToString()));
                field.PutDiscAt(1, selectedColumn - 1);

                if (field.CheckForWin() != null)
                {
                    Console.WriteLine();
                    Console.WriteLine("Winner is: " + field.CheckForWin().Color);
                    return;
                }

            } while (!isOver);
        }
    }
}
