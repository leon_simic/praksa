using System;

namespace Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool playAgain = true;
            bool validInputGameMode;

            do
            {
                Console.WriteLine("Enter which game mode you want to play. (1/2)");
                Console.WriteLine("1 - 1 player");
                Console.WriteLine("2 - 2 players"); 
                validInputGameMode = int.TryParse(Console.ReadLine(), out int gameMode);

                if (!validInputGameMode || gameMode < 1 || gameMode > 2)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input.");
                    continue;
                }

                if (gameMode == 1)
                    playAgain = RunOnePlayerMode();
                else
                    playAgain = RunTwoPlayerMode();
            } while (playAgain);
        }

        public static bool RunOnePlayerMode()
        {
            Board board = new Board();
            Coin[] coins = new Coin[]
            {
                new Coin(PlayerType.PlayerA),
                new Coin(PlayerType.PlayerB)
            };
            AI aI = new AI();

            bool isColumnFull;
            Console.Clear();

            while (true)
            {
                board.Print();
                Console.WriteLine();
                Console.WriteLine("Enter column number: ");

                bool validColumnInput = int.TryParse(Console.ReadLine(), out int selectedColumn);
                isColumnFull = !board.InsertCoinAt(coins[0], selectedColumn);

                if (!validColumnInput || selectedColumn <= 0 || selectedColumn > board.Columns || isColumnFull)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input.");
                    continue;
                }

                aI.PlayMove(board, coins);

                if (board.CheckForWin() != 0)
                {
                    Console.Clear();
                    board.Print();

                    if (board.CheckForWin() == 1)
                        Console.WriteLine("You are the winner!");
                    else if (board.CheckForWin() == 2)
                        Console.WriteLine("You lost!");

                    if (IsGameOver())
                        return false;
                    else
                    {
                        Console.Clear();
                        board.Clear();
                        return true;
                    }
                }
                else if (board.IsFull())
                {
                    Console.Clear();
                    board.Print();
                    Console.WriteLine("The board is full. It's a tie!");
                    if (IsGameOver())
                        return false;
                    else
                    {
                        Console.Clear();
                        board.Clear();
                        return true;
                    }
                }
                else
                    Console.WriteLine();
                    Console.Clear();
            }
           
        }

        public static bool RunTwoPlayerMode()
        {
            Board board = new Board();
            Coin[] coins = new Coin[]
            {
                new Coin(PlayerType.PlayerA),
                new Coin(PlayerType.PlayerB)
            };

            bool isColumnFull;
            int coinIndex;

            Console.Clear();
            while (true)
            {
                coinIndex = 0;

                foreach (var player in Enum.GetNames(typeof(PlayerType)))
                {
                    while (true)
                    {
                        board.Print();
                        Console.WriteLine();
                        Console.WriteLine($"{player} is on turn.");
                        Console.WriteLine("Enter column number: ");

                        bool validColumnInput = int.TryParse(Console.ReadLine(), out int selectedColumn);
                        isColumnFull = !board.InsertCoinAt(coins[coinIndex], selectedColumn);

                        if (!validColumnInput || selectedColumn <= 0 || selectedColumn > board.Columns || isColumnFull)
                        {
                            Console.Clear();
                            Console.WriteLine("Invalid input.");
                            continue;
                        }

                        if (board.CheckForWin() != 0)
                        {
                            Console.Clear();
                            board.Print();
                            Console.WriteLine($"Winner is:{player}!");

                            if (IsGameOver())
                                return false;
                            else
                            {
                                Console.Clear();
                                board.Clear();
                                return true;
                            }
                        }
                        else if (board.IsFull())
                        {
                            Console.Clear();
                            board.Print();
                            Console.WriteLine("The board is full. It's a tie!");
                            if (IsGameOver())
                                return false;
                            else
                            {
                                Console.Clear();
                                board.Clear();
                                return true;
                            }
                        }
                        else
                            Console.Clear();

                        break;

                    } 
                    coinIndex++;
                }
            }
        }

        public static bool IsGameOver()
        {
            bool validInput;
            char input;

            do
            {
                Console.WriteLine("Do you want to play again? (y/n): ");
                validInput = char.TryParse(Console.ReadLine(), out input);
                input = char.ToLower(input);
            } while ((input != 'n' && input != 'y') || !validInput);

            if (input == 'y')
                return false;

            else
                return true;
        }
    }
}
