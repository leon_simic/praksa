using System;

namespace Connect_4
{
    internal class AI
    {
        public void PlayMove(Board board, Coin[] coins)
        {
            Random rand = new Random();
            int randColumn;
            bool validColumn;

            for (int i = board.Rows - 2; i >= 0; i--)
            {
                for (int j = board.Columns; j >= 0; j--)
                {
                    if (board.InsertCoinAt(coins[1], j))
                        if (board.CheckForWin() == 2)
                        {
                            return;
                        }
                        else
                        {
                            board.RemoveCoinAt(j);
                        }

                    if (board.InsertCoinAt(coins[0], j))
                    {
                        if (board.CheckForWin() == 1)
                        {
                            board.RemoveCoinAt(j);
                            board.InsertCoinAt(coins[1], j);
                            return;
                        }
                        else
                        {
                            board.RemoveCoinAt(j);
                        }
                    }
                }
            }

            do
            {
                randColumn = rand.Next(1, 8);
                validColumn = board.InsertCoinAt(coins[1], randColumn);
            } while (!validColumn);
            
        }
    }
}
