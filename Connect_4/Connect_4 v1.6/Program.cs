using System;

namespace Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Board board = new Board();
            Coin[] coins = new Coin[]
            {
                new Coin(PlayerType.PlayerA),
                new Coin(PlayerType.PlayerB)
            };

            bool playAgain;

            do
            {
                playAgain = Run(board, coins);
            } while (playAgain);

        }

        public static bool Run(Board board, Coin[] coins)
        {
            bool isColumnFull;
            int coinIndex;

            while (true)
            {
                isColumnFull = false;
                coinIndex = 0;

                foreach (var player in Enum.GetNames(typeof(PlayerType)))
                {
                    while (true)
                    {
                        board.Print();
                        Console.WriteLine();
                        Console.WriteLine($"{player} is on turn.");
                        Console.WriteLine("Enter column number: ");

                        bool validColumnInput = int.TryParse(Console.ReadLine(), out int selectedColumn);
                        isColumnFull = !board.InsertCoinAt(coins[coinIndex], selectedColumn);

                        if (!validColumnInput || selectedColumn <= 0 || selectedColumn > board.Columns || isColumnFull)
                        {
                            Console.Clear();
                            Console.WriteLine("Invalid input.");
                            continue;
                        }

                        if (board.CheckForWin() != 0)
                        {
                            Console.Clear();
                            board.Print();
                            Console.WriteLine($"Winner is:{player}!");

                            if (IsGameOver())
                                return false;
                            else
                            {
                                Console.Clear();
                                board.Clear();
                                return true;
                            }
                        }
                        else if (board.IsFull())
                        {
                            Console.Clear();
                            board.Print();
                            Console.WriteLine("The board is full. It's a tie!");
                            if (IsGameOver())
                                return false;
                            else
                            {
                                Console.Clear();
                                board.Clear();
                                return true;
                            }
                        }
                        else
                            Console.Clear();

                        break;

                    } 
                    coinIndex++;
                }
            }
        }

        public static bool IsGameOver()
        {
            bool validInput;
            char input;

            do
            {
                Console.WriteLine("Do you want to play again? (y/n): ");
                validInput = char.TryParse(Console.ReadLine(), out input);
                input = char.ToLower(input);
            } while ((input != 'n' && input != 'y') || !validInput);

            if (input == 'y')
                return false;

            else
                return true;
        }
    }
}
