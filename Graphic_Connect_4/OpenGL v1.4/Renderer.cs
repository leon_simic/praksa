using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Renderer
    {
        List<DrawableObject> drawables;
        public Renderer()
        {
            drawables = new List<DrawableObject>();
        }

        public Renderer(List<DrawableObject> drawables)
        {
            this.drawables = drawables;
        }

        public void Add(DrawableObject drawable)
        {
            drawables.Add(drawable);
        }

        public void Draw()
        {
            foreach (var drawable in drawables)
            {
                GL.LoadIdentity();
                drawable.Draw();
            }
        }
    }
}
