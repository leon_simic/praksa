using OpenTK;

namespace OpenTk
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GameWindow gameWindow = new GameWindow();
            Window window = new Window(gameWindow);
        }
    }
}
