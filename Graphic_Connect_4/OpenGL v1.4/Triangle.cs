using OpenTK.Graphics.OpenGL;
using System;

namespace OpenTk
{
    internal class Triangle : DrawableObject
    { 
        public int X { get; private set; }
        public int Y { get; private set; }
        public static int Size { get; private set; }

        public Triangle(int x, int y)
        {
            X = x;
            Y = y;
            Size = 30;
            
        }
        public override void Draw()
        {
            GL.Translate(X, Y, 0);

            GL.Begin(PrimitiveType.Triangles);
            
            GL.Color3(1.0, 1.0, 0.0);
            GL.Vertex2(-Size, -Size);
            GL.Vertex2(Size, -Size);
            GL.Vertex2(0, Size);

            GL.End();
        }
    }
}