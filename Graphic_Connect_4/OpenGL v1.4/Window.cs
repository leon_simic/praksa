using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Window
    {
        GameWindow gameWindow;
        Renderer drawer;

        public Window(GameWindow gameWindow)
        {
            this.gameWindow = gameWindow;
            drawer = new Renderer();
            for (int i = 30; i < 30 * 10; i += 60)
            {
                drawer.Add(new Triangle(i, 30));
            }
            
            Start();
        }

        private void Start()
        {
            gameWindow.Load += LoadedEventHandler;
            gameWindow.Resize += ResizeEventHandler;
            gameWindow.RenderFrame += RenderFrameEventHandler;
            gameWindow.Run();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, gameWindow.Width, gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, gameWindow.Width, 0, gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            //GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.LoadIdentity();
            drawer.Draw();
            gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
