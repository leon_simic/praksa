using System;
using System.Collections.Generic;

namespace OpenTk
{
    internal class Board
    {
        const int ringOuterRadius = 40;
        const int ringInnerRadius = 20;
        const int offset = 10;

        float CurrentX;
        float CurrentY;

        public float Width { get; private set; }
        public float Height { get; private set; }
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Vector Center { get; private set; }
        public List<Quad> Quads { get; private set; }
        public List<Ring>[] Rings { get; private set; }
        public List<Ring> HelpRings { get; private set; }


        public Board(Vector center)
        {
            Rows = 6;
            Columns = 7;
            Center = center;

            CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
            CurrentY = Center.Y - 2 * (2 * ringOuterRadius) - 1.5f * offset;
        }

        public void Create()
        {
            Quads = new List<Quad>();
            Rings = new List<Ring>[Rows];
            HelpRings = new List<Ring>();

            for (int i = 0; i < Rows; i++)
            {
                Rings[i] = new List<Ring>();
            }

            Ring ring;
            Ring helpRing;

            float currentHelpPositionX = Center.X - 2.5f * (2 * ringOuterRadius - offset); 
            float currentHelpPositionY = Center.Y - 2 * (2 * ringOuterRadius - offset);


            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    ring = new Ring(ringOuterRadius, ringInnerRadius, 50, new Vector(CurrentX, CurrentY), Color.Blue);
                    Rings[i].Add(ring);
                    CurrentX += 2 * ringOuterRadius - offset;

                    if (j < 6 && i < 5)
                    {
                        helpRing = new Ring(ringInnerRadius, 0, 30, new Vector(currentHelpPositionX, currentHelpPositionY), Color.Blue);
                        HelpRings.Add(helpRing);
                        currentHelpPositionX += 2 * ringOuterRadius - offset;
                    }
                }
                CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
                CurrentY += 2 * ringOuterRadius - offset;

                currentHelpPositionX = Center.X - 2.5f * (2 * ringOuterRadius - offset);
                currentHelpPositionY += 2 * ringOuterRadius - offset;
            }

            CreateQuads();
        }

        private void CreateQuads()
        {
            int verticalQuadHeight = Rows * (2 * ringOuterRadius - offset) + offset;
            int horizontalQuadWidth = Columns * (2 * ringOuterRadius - offset) + offset;
            int quadSize = 30;

            Quad horizontalQuad = new Quad(horizontalQuadWidth, quadSize, new Vector(Center.X, Center.Y - 2.5f * (2 * ringOuterRadius) - offset), Color.Blue);
            Quads.Add(horizontalQuad);

            Quad horizontalQuad2 = new Quad(horizontalQuadWidth, quadSize, new Vector(Center.X, Center.Y + 2.5f * (2 * ringOuterRadius) + offset), Color.Blue);
            Quads.Add(horizontalQuad2);

            Quad verticalQuad = new Quad(quadSize, verticalQuadHeight + quadSize - offset, new Vector(Center.X - 3 * (2 * ringOuterRadius) - 0.5f * offset, Center.Y), Color.Blue);
            Quads.Add(verticalQuad);

            Quad verticalQuad2 = new Quad(quadSize, verticalQuadHeight + quadSize - offset, new Vector(Center.X + 3 * (2 * ringOuterRadius) + 0.5f * offset, Center.Y), Color.Blue);
            Quads.Add(verticalQuad2);


            Width = 7 * (2 * ringOuterRadius - offset) + quadSize;
            Height = 6 * (2 * ringOuterRadius - offset) + quadSize;
        }

        public bool IsFull()
        {
            int count = 0;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Rings[i][j].IsPopulated())
                        count++;
                }
            }

            return count == Rows * Columns;
        }

        public void Clear()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Rings[i][j] = null;
                }
            }
        }

        public int CheckForWin()
        {
            if (CheckRows() != 0)
                return CheckRows();


            if (CheckColumns() != 0)
                return CheckColumns();


            if (CheckMajorDiagonals() != 0)
                return CheckMajorDiagonals();


            if (CheckMinorDiagonals() != 0)
                return CheckMinorDiagonals();

            return 0;
        }

        private int CheckForAWinner(Coin[] coins)
        {
            int countA = 0;
            int countB = 0;

            for (int i = 0; i < coins.Length; i++)
            {
                if (coins[i] == null)
                {
                    countA = 0;
                    countB = 0;
                }
                else if (coins[i].Type == PlayerType.PlayerA)
                {
                    countA++;
                    countB = 0;
                }
                else
                {
                    countB++;
                    countA = 0;
                }

                if (countA >= 4)
                    return 1;
                else if (countB >= 4)
                    return 2;
            }

            return 0;
        }

        private int CheckRows()
        {
            Coin[] coins = new Coin[Rows];

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Rings[i][j].IsPopulated())
                        coins[i] = Rings[i][j].Coin;
                }

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckColumns()
        {
            Coin[] coins = new Coin[Columns];

            for (int i = 0; i < Columns; i++)
            {
                for (int j = 0; j < Rows; j++)
                {
                    if (Rings[j][i].IsPopulated())
                        coins[j] = Rings[j][i].Coin;
                }

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMinorDiagonals()
        {
            Coin[] coins = new Coin[Rows];

            int sum = 3;
            int newIndex = 0;
            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        if (i + j == sum)
                        {
                            coins[newIndex] = Rings[i][j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                sum++;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMajorDiagonals()
        {
            Coin[] coins = new Coin[Rows];
            int difference = 2;
            int newIndex = 0;

            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = Rows - 1; i >= 0; i--)
                {
                    for (int j = Columns - 1; j >= 0; j--)
                    {
                        if (i - j == difference)
                        {
                            coins[newIndex] = Rings[i][j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                difference--;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        public void OnResize(int width, int height)
        {
            Center = new Vector(width / 2, height / 2);

            CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
            CurrentY = Center.Y - 2 * (2 * ringOuterRadius) - 1.5f * offset;

            Create();
        }
    }
}
