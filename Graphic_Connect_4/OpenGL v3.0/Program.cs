using OpenTK;
using System;

namespace OpenTk
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool playAgain = true;
            bool validInputGameMode;

            do
            {
                Console.WriteLine("Enter which game mode you want to play. (1/2)");
                Console.WriteLine("1 - 1 player");
                Console.WriteLine("2 - 2 players");
                validInputGameMode = int.TryParse(Console.ReadLine(), out int gameMode);

                if (!validInputGameMode || gameMode < 1 || gameMode > 2)
                {
                    Console.Clear();
                    Console.WriteLine("Invalid input.");
                    continue;
                }

                if (gameMode == 1)
                    playAgain = RunOnePlayerMode();
                else
                    playAgain = RunTwoPlayerMode();
            } while (playAgain);
        }

        private static bool RunTwoPlayerMode()
        {
            GameWindow practice = new GameWindow();
            Window gameWindow = new Window(practice);

            gameWindow.Start();

            return false;
        }

        private static bool RunOnePlayerMode()
        {
            throw new NotImplementedException();
        }
    }
}
