using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace OpenTk
{
    internal class Board
    {
        public float Width { get; private set; }
        public float Height { get; private set; }

        public Renderer Create(Window window)
        {
            Renderer renderer = new Renderer();
            Ring ring;
            Ring helpRing;
            int offset = 10;

            int ringOuterRadius = 40;
            int ringInnerRadius = 20;

            int currentX = ringOuterRadius + ringInnerRadius / 2;
            int currentY = ringOuterRadius + ringInnerRadius / 2;

            int verticalQuadHeight = 6 * (2 * ringOuterRadius - offset) + offset;
            int horizontalQuadWidth = 7 * (2 * ringOuterRadius - offset) + offset;
            int quadSize = 30;

            int currentHelpPositionX = 2 * ringOuterRadius + offset / 2; 
            int currentHelpPositionY = 2 * ringOuterRadius + offset / 2;
             

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    ring = new Ring(ringOuterRadius, ringInnerRadius, 50, new Vector(currentX, currentY), Color.Blue);
                    renderer.Add(ring);
                    currentX += 2 * ringOuterRadius - offset;

                    if (j < 6 && i < 5)
                    {
                        helpRing = new Ring(ringInnerRadius, 0, 30, new Vector(currentHelpPositionX, currentHelpPositionY), Color.Blue);
                        renderer.Add(helpRing);
                        currentHelpPositionX += 2 * ringOuterRadius - offset;
                    }
                }
                currentX = ringOuterRadius + ringInnerRadius / 2;
                currentY += 2 * ringOuterRadius - offset;

                currentHelpPositionX = 2 * ringOuterRadius + offset / 2;
                currentHelpPositionY += 2 * ringOuterRadius - offset;

            }

            Quad horizontalQuad = new Quad(quadSize, horizontalQuadWidth, new Vector(0, 0), Color.Blue);
            renderer.Add(horizontalQuad);

            Quad horizontalQuad2 = new Quad(quadSize, horizontalQuadWidth, new Vector(0, verticalQuadHeight - offset), Color.Blue);
            renderer.Add(horizontalQuad2);

            Quad verticalQuad = new Quad(verticalQuadHeight, quadSize, new Vector(0, 0), Color.Blue);
            renderer.Add(verticalQuad);

            Quad verticalQuad2 = new Quad(verticalQuadHeight + ringOuterRadius / 2, 30, new Vector(horizontalQuadWidth - offset, 0), Color.Blue);
            renderer.Add(verticalQuad2);

            Width = verticalQuad2.Position.X + quadSize - verticalQuad.Position.X;
            Height = horizontalQuad2.Position.Y + quadSize - horizontalQuad.Position.Y;

            window.OnResize += () =>
            {


            };

            return renderer;
        }
    }
}
