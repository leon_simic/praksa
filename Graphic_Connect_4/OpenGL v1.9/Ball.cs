using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Ball : Circle, IUpdatable
    {
        public Ball(float radius, int segments, Vector position, Color color) : base(radius, segments, position, color) { }

        public void OnUpdate(float deltaTime)
        {
            GL.LoadIdentity();

            Position = new Vector(Position.X, deltaTime * 60);
            Draw();
        }
    }
}
