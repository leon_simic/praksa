namespace OpenTk
{
    internal interface IUpdatable
    {
        void OnUpdate(float deltaTime);
    }
}
