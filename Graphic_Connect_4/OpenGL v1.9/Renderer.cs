using System.Linq;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Renderer
    {
        List<IDrawable> drawables;
        public Renderer()
        {
            drawables = new List<IDrawable>();
        }

        public Renderer(List<IDrawable> drawables)
        {
            this.drawables = drawables;
        }

        public void Add(IDrawable drawable)
        {
            drawables.Add(drawable);
        }
        public void SortByLayer()
        {
            drawables = drawables.OrderBy(x => x.Layer).ToList();
        }

        public void Draw()
        {
            foreach (var drawable in drawables)
            {
                GL.LoadIdentity();
                drawable.Draw();
                GL.Color3(1f, 1f, 1f);
            }
        }
    }
}
