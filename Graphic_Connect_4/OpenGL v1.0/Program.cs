using System;
using OpenTK;

namespace OpenTk
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GameWindow gameWindow = new GameWindow();
            Game game = new Game(gameWindow);
        }
    }
}
