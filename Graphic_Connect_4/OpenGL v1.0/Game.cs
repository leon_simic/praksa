using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Game
    {
        GameWindow gameWindow;

        public Game(GameWindow gameWindow)
        {
            this.gameWindow = gameWindow;
            Start();
        }

        private void Start()
        {
            gameWindow.Load += LoadedEventHandler;
            gameWindow.Resize += ResizeEventHandler;
            gameWindow.RenderFrame += RenderFrameEventHandler;
            gameWindow.Run();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, gameWindow.Width, gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, gameWindow.Width, 0, gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.Begin(PrimitiveType.Triangles);

            GL.Color3(0.0, 0.0, 1.0);
            GL.Vertex2(20, 10);
            GL.Vertex2(200, 10);
            GL.Vertex2(100, 200);

            GL.End();
            gameWindow.SwapBuffers();
        }

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
