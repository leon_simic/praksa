using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Triangle : DrawableObject
    {
        public override void Draw()
        {
            GL.Begin(PrimitiveType.Triangles);

            GL.Color3(0.0, 0.0, 1.0);
            GL.Vertex2(20, 10);
            GL.Vertex2(200, 10);
            GL.Vertex2(100, 200);

            GL.End();
        }
    }
}
