using System;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Circle : DrawableObject
    {
        public float Radius { get; private set; }
        public int Segments {  get; private set; } 
        public Vector Position { get; set; }
        public Color Color { get; private set; }

        public Circle(float radius, int segments, Vector position, Color color) : base()
        {
            Radius = radius;
            Segments = segments;
            Position = position;
            Color = color;
        }

        public override void Draw()
        {
            double angle;

            GL.Begin(PrimitiveType.TriangleFan);
            GL.Color3(Color.R, Color.G, Color.B);

            for (int i = 0; i <= Segments; i++)
            {
                angle = i * 2 * Math.PI / Segments;
                GL.Vertex2(Position.X + Radius * Math.Cos(angle),
                    Position.Y + Radius * Math.Sin(angle));
            }

            GL.End();
        }
    }
}
