using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Window
    {
        GameWindow _gameWindow;
        public GameWindow MainWindow => _gameWindow;

        Renderer renderer;
        Texture crate;
        Texture brick;
        Texture grass;
        Board board;

        public event Action OnResize;

        public Window(GameWindow gameWindow)
        {
            this._gameWindow = gameWindow;

            board = new Board();
            renderer = board.Create(this);

            Start();
        }

        public void Practice()
        {
            renderer = new Renderer();

            crate = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\crateTexture.bmp");
            brick = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\brickTexture.bmp");
            grass = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\grassTexture.bmp");

            Triangle triangle = new Triangle(30, new Vector(400, 400), Color.Red);
            triangle.Layer = 1;
            renderer.Add(triangle);

            Circle circle = new Circle(100, 40, new Vector(100, 100), Color.Green);
            circle.Layer = 1;
            renderer.Add(circle);

            Ring ring = new Ring(100, 50, 40, new Vector(300, 200), Color.Blue);
            ring.Layer = 1;
            renderer.Add(ring);

            Quad quad = new Quad(50, 150, new Vector(500, 200), Color.Red);
            quad.Layer = 1;
            renderer.Add(quad);

            TextureQuad crateQuad = new TextureQuad(200, 100, new Vector(500, 70), crate);
            crateQuad.Layer = 1;
            renderer.Add(crateQuad);

            TextureQuad brickQuad = new TextureQuad(100, 98, new Vector(300, 70), brick);
            brickQuad.Layer = 1;
            renderer.Add(brickQuad);


            TextureQuad grassQuad = new TextureQuad(200, 150, new Vector(150, 400), grass);
            renderer.Add(grassQuad);

            renderer.SortByLayer();
        }

        private void Start()
        {
            _gameWindow.Load += LoadedEventHandler;
            _gameWindow.Resize += ResizeEventHandler;
            _gameWindow.RenderFrame += RenderFrameEventHandler;
            _gameWindow.Run();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport((int)(_gameWindow.Width / 2 - board.Width / 2), (int)(_gameWindow.Height / 2 - board.Height / 2), _gameWindow.Width, _gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity(); 
            GL.Ortho(0, _gameWindow.Width, 0, _gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);
            Console.WriteLine(_gameWindow.Width);
            //OnResize.Invoke();
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.LoadIdentity();
            renderer.Draw();
            _gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
