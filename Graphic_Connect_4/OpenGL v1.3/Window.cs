using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Window
    {
        GameWindow gameWindow;
        Drawer drawer;

        public Window(GameWindow gameWindow)
        {
            this.gameWindow = gameWindow;
            drawer = new Drawer();
            for (int i = 0; i < 12; i++)
            {
                drawer.Add(new Triangle());
            }

            Start();
        }

        private void Start()
        {
            gameWindow.Load += LoadedEventHandler;
            gameWindow.Resize += ResizeEventHandler;
            gameWindow.RenderFrame += RenderFrameEventHandler;
            gameWindow.Run();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, gameWindow.Width, gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, gameWindow.Width, 0, gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            //GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.LoadIdentity();
            drawer.Draw();
            gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
