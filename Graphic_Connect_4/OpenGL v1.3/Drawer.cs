using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Drawer
    {
        List<DrawableObject> drawables;
        public Drawer()
        {
            drawables = new List<DrawableObject>();
        }

        public Drawer(List<DrawableObject> drawables)
        {
            this.drawables = drawables;
        }

        public void Add(DrawableObject drawable)
        {
            drawables.Add(drawable);
        }

        public void Draw()
        {
            int k = 0;
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    drawables[i].Draw();
                    GL.Translate(Triangle.Size * 2 + 5, 0, 0);
                }
                GL.LoadIdentity();
                GL.Translate(0, Triangle.Size * (k + 2) + 5, 0);
                k += 2;
            }

            //foreach (var drawable in drawables)
            //{
            //    drawable.Draw();
            //    GL.Translate(Triangle.Size * 2 + 5, 0, 0);
            //}
        }
    }
}
