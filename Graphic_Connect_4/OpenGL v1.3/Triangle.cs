using OpenTK.Graphics.OpenGL;
using System;

namespace OpenTk
{
    internal class Triangle : DrawableObject
    { 
        public int X { get; private set; }
        public int Y { get; private set; }
        public static int Size { get; private set; }

        public Triangle()
        {
            X = 40;
            Y = 40;
            Size = 30;
            
        }
        public override void Draw()
        {
            GL.Begin(PrimitiveType.Triangles);
            
            GL.Color3(1.0, 1.0, 0.0);
            GL.Vertex2(X - Size, Y - Size);
            GL.Vertex2(X + Size, Y - Size);
            GL.Vertex2(X, Y + Size);

            GL.End();
        }
    }
}