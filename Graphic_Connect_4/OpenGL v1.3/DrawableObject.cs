namespace OpenTk
{
    internal abstract class DrawableObject
    {
        public abstract void Draw();
    }
}
