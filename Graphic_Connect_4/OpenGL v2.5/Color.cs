namespace OpenTk
{
    internal struct Color
    {
        public float R { get; private set; }
        public float G { get; private set; }
        public float B { get; private set; }

        public Color(byte r, byte g, byte b)
        {
            R = (float)r / 255;
            G = (float)g / 255;
            B = (float)b / 255;
        }

        public Color(float r, float g, float b)
        {
            R = r;
            G = g;
            B = b;
        }

        public static Color Red
        {
            get
            {
                return new Color(1f, 0, 0);
            }
        }

        public static Color Green
        {
            get
            {
                return new Color(0, 1f, 0);
            }
        }

        public static Color Blue
        {
            get
            {
                return new Color(0, 0, 1f);
            }
        }
        public static Color Yellow
        {
            get
            {
                return new Color(1f, 1f, 0);
            }
        }

        public static Color Black
        {
            get
            {
                return new Color(0f, 0f, 0f);
            }
        }
    }
}
