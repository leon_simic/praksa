using System;

namespace OpenTk
{
    internal class Coin : Circle, IUpdatable
    {
        public float PositionOffset { get; set; }
        public Vector EndPosition { get; set; }
        public bool IsMoving { get; private set; }


        public Coin(float radius, int segments, Vector startPosition, Vector endPosition, Color color) : base(radius, segments, startPosition, color) 
        {
            PositionOffset = 1000;
            EndPosition = endPosition;
            Layer = 1;
        }

        public void OnUpdate(float deltaTime)
        {
            if (Position != EndPosition)
            { 
                Position = new Vector(Position.X, (int)(Position.Y - deltaTime * PositionOffset));
                IsMoving = true;
            }
            if (Math.Pow(EndPosition.X - Position.X, 2) + Math.Pow(EndPosition.Y - Position.Y, 2) <= Math.Pow(deltaTime * PositionOffset, 2))
            {
                Position = EndPosition;
                IsMoving = false;
            }
            Draw();
        }
    }
}
