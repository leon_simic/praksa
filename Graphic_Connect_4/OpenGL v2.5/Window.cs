using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace OpenTk
{
    internal class Window
    {
        delegate void OnResize(int width, int height);

        GameWindow _gameWindow;
        public GameWindow MainWindow => _gameWindow;

        Renderer renderer;
        Texture crate;
        Texture brick;
        Texture grass;
        Board board; 
        Coin coin;
        float deltaTime;
        OnResize boardOnResize;
        MouseState mouse;
        List<bool>[] copyBoard;

        float mouseInWindowX;
        float mouseInWindowY;

        public Window(GameWindow gameWindow)
        {
            deltaTime = 1f / 60;
            _gameWindow = gameWindow;
            renderer = new Renderer(deltaTime);

            board = new Board(new Vector(_gameWindow.Width / 2, _gameWindow.Height / 2));

            boardOnResize = new OnResize(board.OnResize);

            copyBoard = new List<bool>[board.Rows];
            for (int i = 0; i < board.Rows; i++)
            {
                copyBoard[i] = new List<bool> { false, false, false, false, false, false, false};
            }

            Start();
        }

        public void Practice()
        {
            renderer = new Renderer(deltaTime);

            crate = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\crateTexture.bmp");
            brick = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\brickTexture.bmp");
            grass = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\grassTexture.bmp");

            Triangle triangle = new Triangle(30, new Vector(400, 400), Color.Red);
            triangle.Layer = 1;
            renderer.Add(triangle);

            Circle circle = new Circle(100, 40, new Vector(100, 100), Color.Green);
            circle.Layer = 1;
            renderer.Add(circle);

            Ring ring = new Ring(100, 50, 40, new Vector(300, 200), Color.Blue);
            ring.Layer = 1;
            renderer.Add(ring);

            Quad quad = new Quad(50, 150, new Vector(500, 200), Color.Red);
            quad.Layer = 1;
            renderer.Add(quad);

            TextureQuad crateQuad = new TextureQuad(200, 100, new Vector(500, 70), crate);
            crateQuad.Layer = 1;
            renderer.Add(crateQuad);

            TextureQuad brickQuad = new TextureQuad(100, 98, new Vector(300, 70), brick);
            brickQuad.Layer = 1;
            renderer.Add(brickQuad);

            TextureQuad grassQuad = new TextureQuad(200, 150, new Vector(150, 400), grass);
            renderer.Add(grassQuad);

        }

        private void Start()
        {
            _gameWindow.Load += LoadedEventHandler; 
            _gameWindow.Resize += ResizeEventHandler;
            _gameWindow.RenderFrame += RenderFrameEventHandler;
            _gameWindow.MouseUp += MouseUpEventHandler;
            _gameWindow.Run();
        }

        private void MouseUpEventHandler(object sender, MouseButtonEventArgs e)
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;

            if (!board.IsFull())
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouseInWindowX, mouseInWindowY))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            Console.WriteLine("start position " + startPosition.X + " " + startPosition.Y);

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated)
                        {

                            coin = new Coin(20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow);
                            board.Rings[i][j].IsPopulated = true;
                            Console.WriteLine("zauzeto: " + i + " " + j);

                            renderer.Add((IUpdatable)coin);

                            Console.WriteLine("end position " + board.Rings[i][j].Position.X + " " + board.Rings[i][j].Position.Y);
                            return;

                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void CreateCopyBoard()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    copyBoard[i][j] = board.Rings[i][j].IsPopulated;
                }
            }

        }

        private void SetBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    board.Rings[i][j].IsPopulated = copyBoard[i][j];
                }
            }
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, _gameWindow.Width, _gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, _gameWindow.Width, 0, _gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);

            if (board.Rings != null)
                CreateCopyBoard();

            boardOnResize.Invoke(_gameWindow.Width, _gameWindow.Height);
            
            renderer.Clear();
            renderer.Add(board);
            SetBoardPopulation();
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.LoadIdentity();

            renderer.Draw();

            mouse = Mouse.GetCursorState();

            mouseInWindowX = mouse.X - _gameWindow.Bounds.X - 8;
            mouseInWindowY = -mouse.Y + _gameWindow.Bounds.Bottom - 10;

            //Console.WriteLine(mouseInWindowX + " " + mouseInWindowY);
            _gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
