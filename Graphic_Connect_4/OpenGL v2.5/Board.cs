using System;
using System.Collections.Generic;

namespace OpenTk
{
    internal class Board
    {
        const int ringOuterRadius = 40;
        const int ringInnerRadius = 20;
        const int offset = 10;

        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public Vector Center { get; private set; }
        public float CurrentX { get; private set; }
        public float CurrentY { get; private set; }
        public List<Quad> Quads { get; private set; }
        public List<Ring>[] Rings { get; private set; }
        public List<Ring> HelpRings { get; private set; }


        public Board(Vector center)
        {
            Rows = 6;
            Columns = 7;
            Center = center;

            CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
            CurrentY = Center.Y - 2 * (2 * ringOuterRadius) - 1.5f * offset;
        }

        public void Create()
        {
            Quads = new List<Quad>();
            Rings = new List<Ring>[Rows];
            HelpRings = new List<Ring>();

            for (int i = 0; i < Rows; i++)
            {
                Rings[i] = new List<Ring>();
            }

            Ring ring;
            Ring helpRing;

            float currentHelpPositionX = Center.X - 2.5f * (2 * ringOuterRadius - offset); 
            float currentHelpPositionY = Center.Y - 2 * (2 * ringOuterRadius - offset);


            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    ring = new Ring(ringOuterRadius, ringInnerRadius, 50, new Vector(CurrentX, CurrentY), Color.Blue);
                    Rings[i].Add(ring);
                    CurrentX += 2 * ringOuterRadius - offset;

                    if (j < 6 && i < 5)
                    {
                        helpRing = new Ring(ringInnerRadius, 0, 30, new Vector(currentHelpPositionX, currentHelpPositionY), Color.Blue);
                        HelpRings.Add(helpRing);
                        currentHelpPositionX += 2 * ringOuterRadius - offset;
                    }
                }
                CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
                CurrentY += 2 * ringOuterRadius - offset;

                currentHelpPositionX = Center.X - 2.5f * (2 * ringOuterRadius - offset);
                currentHelpPositionY += 2 * ringOuterRadius - offset;
            }

            CreateQuads();

            //window.OnResize += () =>
            //{


            //};
        }

        private void CreateQuads()
        {
            int verticalQuadHeight = Rows * (2 * ringOuterRadius - offset) + offset;
            int horizontalQuadWidth = Columns * (2 * ringOuterRadius - offset) + offset;
            int quadSize = 30;

            Quad horizontalQuad = new Quad(horizontalQuadWidth, quadSize, new Vector(Center.X, Center.Y - 2.5f * (2 * ringOuterRadius) - offset), Color.Blue);
            Quads.Add(horizontalQuad);

            Quad horizontalQuad2 = new Quad(horizontalQuadWidth, quadSize, new Vector(Center.X, Center.Y + 2.5f * (2 * ringOuterRadius) + offset), Color.Blue);
            Quads.Add(horizontalQuad2);

            Quad verticalQuad = new Quad(quadSize, verticalQuadHeight + quadSize - offset, new Vector(Center.X - 3 * (2 * ringOuterRadius) - 0.5f * offset, Center.Y), Color.Blue);
            Quads.Add(verticalQuad);

            Quad verticalQuad2 = new Quad(quadSize, verticalQuadHeight + quadSize - offset, new Vector(Center.X + 3 * (2 * ringOuterRadius) + 0.5f * offset, Center.Y), Color.Blue);
            Quads.Add(verticalQuad2);
        }

        public bool IsFull()
        {
            int count = 0;

            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    if (Rings[i][j].IsPopulated)
                        count++;
                }
            }

            return count == Rows * Columns;
        }

        public void OnResize(int width, int height)
        {
            Center = new Vector(width / 2, height / 2);

            CurrentX = Center.X - 2.5f * (2 * ringOuterRadius) - offset;
            CurrentY = Center.Y - 2 * (2 * ringOuterRadius) - 1.5f * offset;

            Create();
        }
    }
}
