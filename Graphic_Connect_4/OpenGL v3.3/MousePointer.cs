using OpenTK;
using OpenTK.Input;

namespace Graphic_Connect_4
{
    internal class WindowMouse
    {
        public MouseState Mouse { get; private set; }
        public float X { get; set; }
        public float Y { get; set; }

        public void SetMouseCoordinates(GameWindow gameWindow)
        {
            Mouse = OpenTK.Input.Mouse.GetCursorState();
            X = Mouse.X - gameWindow.Bounds.X - 8;
            Y = -Mouse.Y + gameWindow.Bounds.Bottom - 10;
        }
    }
}
