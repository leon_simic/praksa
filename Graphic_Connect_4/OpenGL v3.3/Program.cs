using OpenTK;
using System;

namespace Graphic_Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GameWindow window = new GameWindow();
            Game game = new Game(window);
            game.Run();
            
        }
    }
}
