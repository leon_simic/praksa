using System;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Ring : DrawableObject
    {
        public float OuterRadius { get; private set; }
        public float InnerRadius { get; private set; }
        public int Segments { get; private set; }
        public Vector Position { get; private set; }
        public Color Color { get; private set; }

        public Ring(float outerRadius, float innerRadius, int segments, Vector position, Color color)
        {
            OuterRadius = outerRadius;
            InnerRadius = innerRadius;
            Segments = segments;
            Position = position;
            Color = color;
        }

        public override void Draw()
        {
            double angle;

            GL.Begin(PrimitiveType.QuadStrip);
            GL.Color3(Color.R, Color.G, Color.B);

            for (int i = 0; i <= Segments; i++)
            {
                angle = 2 * Math.PI * i / Segments;
                GL.Vertex2(Position.X + Math.Cos(angle) * InnerRadius, Position.Y + 30 + Math.Sin(angle) * InnerRadius);
                GL.Vertex2(Position.X + Math.Cos(angle) * OuterRadius, Position.Y + 30 + Math.Sin(angle) * OuterRadius);
            }

            GL.End();
        }
    }
}
