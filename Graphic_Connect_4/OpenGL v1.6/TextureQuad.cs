using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class TextureQuad : DrawableObject
    {
        public float A { get; private set; }
        public float B { get; private set; }
        public Vector Position { get; private set; }
        public Texture Texture { get; private set; }

        public TextureQuad(float a, float b, Vector position, Texture texture) : base()
        {
            A = a;
            B = b;
            Position = position;
            Texture = texture;
        }

        public override void Draw()
        {
            GL.Translate(Position.X, Position.Y, 0);

            Texture.RenderTexture();

            GL.Begin(PrimitiveType.Quads);

            GL.TexCoord2(1, 1);
            GL.Vertex2(A / 2, B / 2);
            
            GL.TexCoord2(0, 1);
            GL.Vertex2(-A / 2, B / 2);

            GL.TexCoord2(0, 0);
            GL.Vertex2(-A / 2, -B / 2);

            GL.TexCoord2(1, 0);
            GL.Vertex2(A / 2, -B / 2);

            GL.End();
        }
    }
}
