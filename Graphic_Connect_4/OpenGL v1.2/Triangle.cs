using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Triangle : DrawableObject
    { 
        public override void Draw()
        {

            GL.Begin(PrimitiveType.Triangles);

            GL.Color3(1.0, 1.0, 0.0);
            GL.Vertex2(10, 10);
            GL.Vertex2(60, 10);
            GL.Vertex2(35, 60);

            GL.End();
            GL.Translate(5, 0, 0);

        }
    }
}
