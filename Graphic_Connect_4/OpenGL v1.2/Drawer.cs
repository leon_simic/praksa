using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Drawer
    {
        List<DrawableObject> drawables;
        public Drawer()
        {
            drawables = new List<DrawableObject>();
        }

        public Drawer(List<DrawableObject> drawables)
        {
            this.drawables = drawables;
        }

        public void Add(DrawableObject drawable)
        {
            drawables.Add(drawable);
        }

        public void Draw()
        {
            foreach (var drawable in drawables)
            {
                drawable.Draw();
            }
        }
    }
}
