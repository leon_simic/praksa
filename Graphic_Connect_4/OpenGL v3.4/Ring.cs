using System;
using OpenTK.Graphics.OpenGL;

namespace Graphic_Connect_4
{
    internal class Ring : IDrawable
    {
        public float OuterRadius { get; private set; }
        public float InnerRadius { get; private set; }
        public int Segments { get; private set; }
        public Vector Position { get; set; }
        public Color Color { get; private set; }
        public int Layer { get; set; }
        public Coin Coin { get; private set; }

        public Ring(float outerRadius, float innerRadius, int segments, Vector position, Color color)
        {
            OuterRadius = outerRadius;
            InnerRadius = innerRadius;
            Segments = segments;
            Position = position;
            Color = color;
        }

        public void Draw()
        {
            double angle;

            GL.Translate(Position.X, Position.Y, 0);

            GL.Begin(PrimitiveType.QuadStrip);
            GL.Color3(Color.R, Color.G, Color.B);

            for (int i = 0; i <= Segments; i++)
            {
                angle = 2 * Math.PI * i / Segments;
                GL.Vertex2(Math.Cos(angle) * InnerRadius, Math.Sin(angle) * InnerRadius);
                GL.Vertex2(Math.Cos(angle) * OuterRadius, Math.Sin(angle) * OuterRadius);
            }

            GL.End();
        }

        public bool Contains(float x, float y)
        {
            if ((x - Position.X) * (x - Position.X) + (y - Position.Y) * (y - Position.Y) <= OuterRadius * OuterRadius)
                return true;

            return false;
        }

        public void Populate(Coin coin)
        {
            Coin = coin;
        }

        public void Clear()
        {
            Coin = null;
        }

        public bool IsPopulated()
        {
            return Coin != null;
        }
    }
}
