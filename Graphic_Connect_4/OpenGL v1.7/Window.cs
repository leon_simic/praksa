using System;
using System.Drawing.Imaging;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Window
    {
        GameWindow gameWindow;
        Renderer renderer;
        Texture crate;
        Texture brick;
        //Texture grass;

        public Window(GameWindow gameWindow)
        {
            GL.Enable(EnableCap.Texture2D);

            this.gameWindow = gameWindow;
            renderer = new Renderer();

            crate = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\crateTexture.bmp");
            brick = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\brickTexture.bmp");
            //grass = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\grassTexture.bmp");

            Triangle triangle = new Triangle(30, new Vector(400, 400), Color.Red());
            triangle.Layer = 1;
            renderer.Add(triangle);

            Circle circle = new Circle(100, 40, new Vector(100, 100), Color.Green());
            circle.Layer = 1;
            renderer.Add(circle);

            Ring ring = new Ring(100, 50, 40, new Vector(300, 200), Color.Blue());
            ring.Layer = 1;
            renderer.Add(ring);

            Quad quad = new Quad(50, 150, new Vector(500, 200), Color.Red());
            quad.Layer = 1;
            renderer.Add(quad);

            TextureQuad crateQuad = new TextureQuad(200, 100, new Vector(500, 70), crate);
            crateQuad.Layer = 1;
            renderer.Add(crateQuad);

            TextureQuad brickQuad = new TextureQuad(100, 98, new Vector(300, 70), brick);
            brickQuad.Layer = 1;
            renderer.Add(brickQuad);
           

            //TextureQuad grassQuad = new TextureQuad(200, 100, new Vector(100, 200), grass);
            //renderer.Add(grassQuad);

            renderer.SortByLayer();
            
            Start();
        }

        private void Start()
        {
            gameWindow.Load += LoadedEventHandler;
            gameWindow.Resize += ResizeEventHandler;
            gameWindow.RenderFrame += RenderFrameEventHandler;
            gameWindow.Run();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, gameWindow.Width, gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, gameWindow.Width, 0, gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);

            GL.LoadIdentity();
            renderer.Draw();
            gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
