namespace OpenTk
{
    internal abstract class DrawableObject
    {
        public int Layer { get; set; }

        public DrawableObject()
        {
            Layer = 0;
        }

        public abstract void Draw();
    }
}
