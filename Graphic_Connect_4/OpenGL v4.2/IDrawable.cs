namespace Graphic_Connect_4
{
    internal interface IDrawable
    {
        int Layer { get; set; } 
        void Draw();
    }
}
