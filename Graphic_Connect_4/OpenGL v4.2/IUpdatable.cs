namespace Graphic_Connect_4
{
    internal interface IUpdatable
    {
        int Layer { get; set; }
        void OnUpdate(float deltaTime);
    }
}
