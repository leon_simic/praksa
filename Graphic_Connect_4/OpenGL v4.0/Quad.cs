using OpenTK.Graphics.OpenGL;

namespace Graphic_Connect_4
{
    internal class Quad : IDrawable
    {
        public float A { get; private set; }
        public float B { get; private set; }
        public Vector Position { get; set; }
        public Color Color { get; private set; }
        public int Layer { get; set; }


        public Quad(float a, float b, Vector position, Color color)
        {
            A = a;
            B = b;
            Position = position;
            Color = color;
        }

        public void Draw()
        {
            GL.Translate(Position.X, Position.Y, 0);

            GL.Begin(PrimitiveType.Quads);

            GL.Color3(Color.R, Color.G, Color.B);

            GL.Vertex2(A / 2, B/ 2);
            GL.Vertex2(-A / 2, B / 2);
            GL.Vertex2(-A / 2, -B / 2);
            GL.Vertex2(A / 2, -B / 2);

            GL.End();
        }
    }
}
