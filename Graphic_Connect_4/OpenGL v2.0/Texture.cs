using System.Drawing;
using System.Drawing.Imaging;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Texture
    {
        public string FilePath { get; private set; }
        public int Id { get; private set; }

        public Texture(string filePath)
        {
            FilePath = filePath;
            GenerateTexture();
        }
        private BitmapData LoadImage()
        {
            Bitmap bitmap = new Bitmap(FilePath);
            Rectangle rectangle = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadOnly, bitmap.PixelFormat);
            bitmap.UnlockBits(bitmapData);

            return bitmapData;
        }

        public void GenerateTexture()
        {
            int texture;

            GL.GenTextures(1, out texture);
            Id = texture;

            GL.BindTexture(TextureTarget.Texture2D, texture);
            BitmapData bitmapData = LoadImage();
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb, bitmapData.Width,
                bitmapData.Height, 0, OpenTK.Graphics.OpenGL.PixelFormat.Bgr, PixelType.UnsignedByte, bitmapData.Scan0);
            GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
            GL.BindTexture(TextureTarget.Texture2D, 0);

        }

        ~Texture()
        {
            //GL.DeleteTexture(Id);
        }
    }
}
