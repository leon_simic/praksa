using OpenTK.Graphics.OpenGL;
using System;

namespace Graphic_Connect_4
{
    internal class TextureQuad : IDrawable
    {
        public float A { get; private set; }
        public float B { get; private set; }
        public Vector Position { get; set; }
        public Texture Texture { get; private set; }
        public int Layer { get; set; }


        public TextureQuad(float a, float b, Vector position, Texture texture)
        {
            A = a;
            B = b;
            Position = position;
            Texture = texture;
        }

        public void Draw()
        {
            GL.Translate(Position.X, Position.Y, 0);

            GL.BindTexture(TextureTarget.Texture2D, Texture.Id);

            GL.Begin(PrimitiveType.Quads);

            GL.TexCoord2(1, 0);
            GL.Vertex2(A / 2, B / 2);
            
            GL.TexCoord2(0, 0);
            GL.Vertex2(-A / 2, B / 2);

            GL.TexCoord2(0, 1);
            GL.Vertex2(-A / 2, -B / 2);

            GL.TexCoord2(1, 1);
            GL.Vertex2(A / 2, -B / 2);

            GL.End();

            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        public bool Contains(float x, float y)
        {
            if (Math.Abs(x - Position.X) <= (A / 2.5) && Math.Abs(y - Position.Y) <= (B / 3.5))
                return true;

            return false;
        }
    }
}
