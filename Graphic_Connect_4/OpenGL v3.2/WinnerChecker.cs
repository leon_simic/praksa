using System;

namespace Graphic_Connect_4
{
    internal class WinnerChecker
    {
        Board board;

        public WinnerChecker(Board board)
        {
            this.board = board; 
        }

        public int CheckForWin()
        {
            if (CheckRows() != 0)
                return CheckRows();


            if (CheckColumns() != 0)
                return CheckColumns();


            if (CheckMajorDiagonals() != 0)
                return CheckMajorDiagonals();


            if (CheckMinorDiagonals() != 0)
                return CheckMinorDiagonals();

            return 0;
        }

        private int CheckForAWinner(Coin[] coins)
        {
            int countA = 0;
            int countB = 0;

            for (int i = 0; i < coins.Length; i++)
            {
                if (coins[i] == null)
                {
                    countA = 0;
                    countB = 0;
                }
                else if (coins[i].Type == PlayerType.PlayerA)
                {
                    countA++;
                    countB = 0;
                }
                else
                {
                    countB++;
                    countA = 0;
                }
                
                if (countA >= 4)
                    return 1;
                else if (countB >= 4)
                    return 2;
            }

            return 0;
        }

        private int CheckRows()
        {
            Coin[] coins = new Coin[board.Columns];

            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (board.Rings[i][j].IsPopulated())
                        coins[j] = board.Rings[i][j].Coin;
                }

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckColumns()
        {
            Coin[] coins = new Coin[board.Rows];

            for (int i = 0; i < board.Columns; i++)
            {
                for (int j = 0; j < board.Rows; j++)
                {
                    if (board.Rings[j][i].IsPopulated())
                        coins[j] = board.Rings[j][i].Coin;
                }

                var result = CheckForAWinner(coins);

                if (result > 0)
                    return result;

                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMinorDiagonals()
        {
            Coin[] coins = new Coin[board.Rows];

            int sum = 3;
            int newIndex = 0;
            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (i + j == sum)
                        {
                            coins[newIndex] = board.Rings[i][j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                sum++;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }

        private int CheckMajorDiagonals()
        {
            Coin[] coins = new Coin[board.Rows];
            int difference = 2;
            int newIndex = 0;

            for (int diagonal = 0; diagonal < 6; diagonal++)
            {
                for (int i = board.Rows - 1; i >= 0; i--)
                {
                    for (int j = board.Columns - 1; j >= 0; j--)
                    {
                        if (i - j == difference)
                        {
                            coins[newIndex] = board.Rings[i][j].Coin;
                            newIndex++;
                        }

                        var result = CheckForAWinner(coins);

                        if (result > 0)
                            return result;
                    }
                }
                difference--;
                newIndex = 0;
                Array.Clear(coins, 0, coins.Length);
            }

            return 0;
        }
    }
}
