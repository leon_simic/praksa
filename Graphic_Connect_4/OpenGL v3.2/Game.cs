using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Graphic_Connect_4
{
    internal class Game
    {     
        delegate void OnResize(int width, int height);
        GameWindow _gameWindow;
        Renderer renderer;
        
        Board board; 
        Coin tempCoin;
        OnResize boardOnResize;
        WindowMouse mouse;
        List<bool>[] boardPopulation;
        Coin[,] coins;
        bool canClick;
        float coinToBoardOffset;

        bool playerATurn;
        bool onePlayerMode;
        bool gameInProgress;
        List<TextureQuad> startText;
        List<TextureQuad> endText;

        public Game(GameWindow gameWindow)
        {
            Initialize(gameWindow);
        }

        private void Initialize(GameWindow gameWindow)
        {
            mouse = new WindowMouse();

            playerATurn = true;
            onePlayerMode = false;
            gameInProgress = false;

            canClick = true;
            float deltaTime = 1f / 60;
            _gameWindow = gameWindow;
            renderer = new Renderer(deltaTime);

            board = new Board(new Vector(_gameWindow.Width / 2, _gameWindow.Height / 2));
            boardOnResize = new OnResize(board.OnResize);

            coins = new Coin[board.Rows, board.Columns];

            boardPopulation = new List<bool>[board.Rows];
            for (int i = 0; i < board.Rows; i++)
            {
                boardPopulation[i] = new List<bool> { false, false, false, false, false, false, false };
            }

            CreateStartText();
            DrawText();
        }

        private void CreateStartText()
        {
            Texture startTextTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\ClickOnGameMode.jpg");
            Texture onePlayerModeTextTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\OnePlayerMode.jpg");
            Texture twoPlayerModeTextTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\TwoPlayerMode.jpg");

            startText = new List<TextureQuad>();
            TextureQuad startTextLine = new TextureQuad(600, 80, new Vector(300, _gameWindow.Height - 20), startTextTexture);
            startText.Add(startTextLine);
            TextureQuad onePlayerModeText = new TextureQuad(230, 70, new Vector(115, startText[0].Position.Y - 50), onePlayerModeTextTexture);
            startText.Add(onePlayerModeText);
            TextureQuad twoPlayerModeText = new TextureQuad(230, 70, new Vector(115, startText[1].Position.Y - 50), twoPlayerModeTextTexture);
            startText.Add(twoPlayerModeText); 
        }

        private void CreateEndText(int winner)
        {
            Texture winnerATexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\WinnerA.jpg");
            Texture winnerBTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\WinnerB.jpg");
            Texture tieTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\Tie.jpg");
            Texture answerYesTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\AnswerYes.jpg");
            Texture answerNoTexture = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\AnswerNo.jpg");

            endText = new List<TextureQuad>();

            if (winner == 1)
            {
                TextureQuad winnerA = new TextureQuad(500, 130, new Vector(220, _gameWindow.Height - 40), winnerATexture);
                endText.Add(winnerA);
            }
            else if (winner == 2)
            {
                TextureQuad winnerB = new TextureQuad(500, 130, new Vector(220, _gameWindow.Height - 40), winnerBTexture);
                endText.Add(winnerB);
            }
            else if (winner == 0)
            {
                TextureQuad tie = new TextureQuad(500, 130, new Vector(220, _gameWindow.Height - 40), tieTexture);
                endText.Add(tie);
            }

            TextureQuad answerYes = new TextureQuad(60, 40, new Vector(90, endText[0].Position.Y - 80), answerYesTexture);
            endText.Add(answerYes);
            TextureQuad answerNo = new TextureQuad(60, 40, new Vector(endText[1].Position.X + 70, endText[0].Position.Y - 80), answerNoTexture);
            endText.Add(answerNo);

            foreach (var line in endText)
            {
                renderer.Add(line);
            } 
        }

        public void Run()
        {
            _gameWindow.Load += LoadedEventHandler; 
            _gameWindow.Resize += ResizeEventHandler;
            _gameWindow.RenderFrame += RenderFrameEventHandler;
            _gameWindow.MouseUp += MouseUpEventHandler;
            _gameWindow.Run();
        }
         
        private void DeleteStartText()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);

            foreach (var quad in startText)
            {
                GL.DeleteTexture(quad.Texture.Id);
            }

            startText.Clear();
            startText.Capacity = 0;

            GL.ClearColor(0, 0, 0, 0);
            GL.LoadIdentity();
        }

        private void DeleteEndText()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);

            foreach (var quad in endText)
            {
                GL.DeleteTexture(quad.Texture.Id);
            }

            endText.Clear();
            endText.Capacity = 0;

            GL.ClearColor(0, 0, 0, 0);
            GL.LoadIdentity();
        }

        private void MouseUpEventHandler(object sender, MouseButtonEventArgs e)
        {

            if (gameInProgress)
            {
                if (onePlayerMode)
                    StartOnePlayerMode();
                else
                    StartTwoPlayerModeMove();
            }
            else if (PickedStartAgain())
            {
                StartAgain();
            }
            else if (PickedGameOver())
            {
                _gameWindow.Close();
            }

            if (PickedOnePlayerMode())
                InitializeOnePlayerMode();
            else if (PickedTwoPlayerMode())
                InitializeTwoPlayerMode();
        }

        private bool PickedGameOver()
        {
            if (endText != null && endText.Count != 0)
                if (endText[2].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private void StartAgain()
        {
            renderer.Clear();
            DeleteEndText();
            Initialize(_gameWindow);
        }

        private bool PickedStartAgain()
        {
            if (endText != null && endText.Count != 0)
                if (endText[1].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private void InitializeTwoPlayerMode()
        {
            Console.WriteLine("drugi");
            onePlayerMode = false;
            gameInProgress = true;

            foreach (var quad in startText)
            {
                renderer.Remove(quad);
            }
            DeleteStartText();
        }

        private void InitializeOnePlayerMode()
        {
            Console.WriteLine("prvi");
            onePlayerMode = true;
            gameInProgress = true;

            foreach (var quad in startText)
            {
                renderer.Remove(quad);
            }
            DeleteStartText();
        }

        private bool PickedOnePlayerMode()
        {
            if (startText.Count != 0 && !gameInProgress)
                if (startText[1].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private bool PickedTwoPlayerMode()
        {
            if (startText.Count != 0 && !gameInProgress)
                if (startText[2].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private void StartTwoPlayerModeMove()
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;
         
            if (canClick)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouse.X, mouse.Y))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated())
                        {
                            tempCoin = playerATurn ?
                                new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow) :
                                new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            renderer.Add((IUpdatable)tempCoin);
                            coins[i, j] = tempCoin;
                            board.Rings[i][j].Populate(tempCoin);

                            playerATurn = !playerATurn;
                            return;
                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void StartOnePlayerMode()
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;

            if (canClick)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouse.X, mouse.Y))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated())
                        {
                            tempCoin = playerATurn ?
                                new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow) :
                                new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            renderer.Add((IUpdatable)tempCoin);
                            board.Rings[i][j].Populate(tempCoin);
                            coins[i, j] = tempCoin;

                            playerATurn = !playerATurn;
                            return;
                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void CreateCopyBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    boardPopulation[i][j] = board.Rings[i][j].IsPopulated();
                }
            }

        }

        private void SetBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    board.Rings[i][j].Populate(coins[i, j]);
                }
            }
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        { 
            GL.Viewport(0, 0, _gameWindow.Width, _gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, _gameWindow.Width, 0, _gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);

            if (board.Rings != null)
                CreateCopyBoardPopulation();

            if (startText.Count != 0)
                UpdateStartTextPosition();

            if (endText != null && endText.Count != 0)
                UpdateEndTextPosition();

            renderer.Remove(board);
            renderer.Clear();

            DrawText();

            SetCoinToBoardOffset();
            boardOnResize.Invoke(_gameWindow.Width, _gameWindow.Height);

            if (gameInProgress)
            {
                SetBoardPopulation();
                UpdateCoinsPosition();
            }
        }

        private void DrawText()
        {
            if (startText.Count != 0)
            {
                foreach (var line in startText)
                {
                    renderer.Add(line);
                }
            }

            if (endText != null)
            {
                foreach (var line in endText)
                {
                    renderer.Add(line);
                }
            }
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit); 
            GL.LoadIdentity();

            if (gameInProgress && !renderer.Contains(board))
                renderer.Add(board);

            if (gameInProgress)
                renderer.Update();

            renderer.Draw();

            CheckForWin();

            mouse.SetMouseCoordinates(_gameWindow);

            _gameWindow.SwapBuffers();
        }

        private void UpdateStartTextPosition()
        {
            startText[0].Position = new Vector(300, _gameWindow.Height - 20);
            startText[1].Position = new Vector(115, startText[0].Position.Y - 50);
            startText[2].Position = new Vector(115, startText[1].Position.Y - 50);
        }

        private void UpdateEndTextPosition()
        {
            endText[0].Position = new Vector(220, _gameWindow.Height - 40);
            endText[1].Position = new Vector(90, endText[0].Position.Y - 80);
            endText[2].Position = new Vector(endText[1].Position.X + 70, endText[0].Position.Y - 80);
        }

        private void UpdateCoinsPosition()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (!coins[i, j].IsMoving)
                        {
                            coins[i, j].Position = board.Rings[i][j].Position;
                            renderer.Add((IDrawable)coins[i, j]);
                        }
                        else
                        {
                            coins[i, j].Position = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y + coinToBoardOffset);
                            coins[i, j].EndPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y);
                            renderer.Add((IUpdatable)coins[i, j]);
                        }
                    }
                }
            }
        }

        private void SetCoinToBoardOffset()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null && coins[i, j].IsMoving)
                        coinToBoardOffset = coins[i, j].Position.Y - board.Rings[0][0].Position.Y;
                }
            }
        }

        private void CheckForWin()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (coins[i, j].IsMoving)
                        {
                            canClick = false;
                            return;
                        }   
                    }
                }
            }
            canClick = true;

            CheckTheWinner();
        }

        private void CheckTheWinner()
        {
            if (board.Rings != null)
            {
                WinnerChecker winnerChecker = new WinnerChecker(board);
                int winner = winnerChecker.CheckForWin();

                if (winner != 0)
                {
                    gameInProgress = false;
                    renderer.Clear();
                    board.Clear();
                    CreateEndText(winner);
                }
                else if (board.IsFull())
                {
                    gameInProgress = false;
                    renderer.Clear();
                    board.Clear();
                    CreateEndText(0);
                }
            }
        }

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}