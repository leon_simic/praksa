using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Circle : DrawableObject
    {
        public float Radius { get; private set; }
        public int Segments {  get; private set; } 
        public Vector Position { get; private set; }
        public Color Color { get; private set; }

        public Circle(float radius, int segments, Vector position, Color color)
        {
            Radius = radius;
            Segments = segments;
            Position = position;
            Color = color;
        }

        public override void Draw()
        {
            GL.Begin(PrimitiveType.TriangleFan);

            GL.Color3(Color.R, Color.G, Color.B);

            for (int i = 0; i <= Segments; i++)
            {
                GL.Vertex2(Position.X + Radius * Math.Cos(i * 2 * Math.PI / Segments),
                    Position.Y + Radius * Math.Sin(i * 2 * Math.PI / Segments));
            }
            
            GL.End();
        }
    }
}
