namespace OpenTk
{
    internal class Ball : Circle, IUpdatable
    {
        public float PositionOffset { get; private set; }

        public Ball(float radius, int segments, Vector position, Color color) : base(radius, segments, position, color) 
        {
            PositionOffset = 50;
            Layer = 1;
        }

        public void OnUpdate(float deltaTime)
        {
            Position = new Vector(Position.X, Position.Y - deltaTime * PositionOffset);
            Draw();
        }
    }
}
