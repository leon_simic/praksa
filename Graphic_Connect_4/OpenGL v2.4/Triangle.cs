using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Triangle : IDrawable
    { 
        public static float Size { get; private set; }
        public Vector Position { get; set; }
        public Color Color { get; private set; }
        public int Layer { get; set; }

        public Triangle(float size, Vector position, Color color)
        {
            Position = new Vector(position.X, position.Y);
            Size = size;
            Color = color;
            
        }
        public void Draw()
        {
            GL.Translate(Position.X, Position.Y, 0);

            GL.Begin(PrimitiveType.Triangles);
            
            GL.Color3(Color.R, Color.G, Color.B);
            GL.Vertex2(-Size, -Size);
            GL.Vertex2(Size, -Size);
            GL.Vertex2(0, Size);

            GL.End();
        }
    }
}