using OpenTK;

namespace OpenTk
{
    internal interface IUpdatable
    {
        int Layer { get; set; }
        void OnUpdate(float deltaTime);
    }
}
