using System.Linq;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Renderer
    {
        float deltaTime;
        public List<IDrawable> Drawables { get; private set; }
        public List<IUpdatable> Updatables { get; private set; }

        public Renderer(float deltaTime)
        {
            this.deltaTime = deltaTime;
            Drawables = new List<IDrawable>();
            Updatables = new List<IUpdatable>();
        }

        public void Add(IDrawable drawable)
        {
            Drawables.Add(drawable);
        }

        public void Add(IUpdatable updatable)
        {
            Updatables.Add(updatable);
        }

        public void Add(Board board)
        { 
            Drawables.AddRange(board.Rings);
            Drawables.AddRange(board.Quads);
            Drawables.AddRange(board.helpRings);
        }

        private void SortByLayer()
        {
            if (Drawables != null) 
                Drawables = Drawables.OrderBy(x => x.Layer).ToList();

            if (Updatables != null)
                Updatables = Updatables.OrderBy(x => x.Layer).ToList();
        }

        public void Clear()
        {
            Drawables.Clear();
            Updatables.Clear();
        }

        public void Draw()
        {
            foreach (var updatable in Updatables)
            {
                GL.LoadIdentity();
                updatable.OnUpdate(deltaTime);
            }

            foreach (var drawable in Drawables)
            {
                GL.LoadIdentity();
                SortByLayer();
                drawable.Draw();
                
            }
        }
    }
}
