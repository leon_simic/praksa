using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace OpenTk
{
    internal class Window
    {
        delegate void OnResize(int width, int height);

        GameWindow _gameWindow;
        public GameWindow MainWindow => _gameWindow;

        Renderer renderer;
        Texture crate;
        Texture brick;
        Texture grass;
        Board board; 
        IUpdatable ball;
        IDrawable circle;
        float deltaTime;
        OnResize boardOnResize;
        MouseState mouse;

        float mouseInWindowX;
        float mouseInWindowY;

        public Window(GameWindow gameWindow)
        {
            deltaTime = 1f / 60;
            _gameWindow = gameWindow;
            renderer = new Renderer(deltaTime);

            board = new Board(new Vector(_gameWindow.Width / 2, _gameWindow.Height / 2));

            boardOnResize = new OnResize(board.OnResize);

            Start();
        }

        public void Practice()
        {
            renderer = new Renderer(deltaTime);

            crate = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\crateTexture.bmp");
            brick = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\brickTexture.bmp");
            grass = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\grassTexture.bmp");

            Triangle triangle = new Triangle(30, new Vector(400, 400), Color.Red);
            triangle.Layer = 1;
            renderer.Add(triangle);

            Circle circle = new Circle(100, 40, new Vector(100, 100), Color.Green);
            circle.Layer = 1;
            renderer.Add(circle);

            Ring ring = new Ring(100, 50, 40, new Vector(300, 200), Color.Blue);
            ring.Layer = 1;
            renderer.Add(ring);

            Quad quad = new Quad(50, 150, new Vector(500, 200), Color.Red);
            quad.Layer = 1;
            renderer.Add(quad);

            TextureQuad crateQuad = new TextureQuad(200, 100, new Vector(500, 70), crate);
            crateQuad.Layer = 1;
            renderer.Add(crateQuad);

            TextureQuad brickQuad = new TextureQuad(100, 98, new Vector(300, 70), brick);
            brickQuad.Layer = 1;
            renderer.Add(brickQuad);

            TextureQuad grassQuad = new TextureQuad(200, 150, new Vector(150, 400), grass);
            renderer.Add(grassQuad);

        }

        private void Start()
        {
            _gameWindow.Load += LoadedEventHandler; 
            _gameWindow.Resize += ResizeEventHandler;
            _gameWindow.RenderFrame += RenderFrameEventHandler;
            _gameWindow.MouseUp += MouseUpEventHandler;
            _gameWindow.Run();
        }

        private void MouseUpEventHandler(object sender, MouseButtonEventArgs e)
        {
            for (int i = 0; i < board.Rings.Count; i++)
            {
                if (board.Rings[i].Contains(mouseInWindowX, mouseInWindowY))
                {
                    if (i == 0 || i % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("1. stupac");
                    }
                    else if (i == 1 || (i - 1) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("2. stupac");
                    }
                    else if (i == 1 || (i - 2) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("3. stupac");
                    }
                    else if (i == 1 || (i - 3) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("4. stupac");
                    }
                    else if (i == 1 || (i - 4) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("5. stupac");
                    }
                    else if (i == 1 || (i - 5) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("6. stupac");
                    }
                    else if (i == 1 || (i - 6) % 7 == 0)
                    {
                        ball = new Ball(20, 50, new Vector(board.Rings[i].Position.X, board.Rings[35].Position.Y), Color.Yellow);
                        renderer.Add(ball);

                        Console.WriteLine("7. stupac");
                    }
                }
            }

        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, _gameWindow.Width, _gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection); 
            GL.LoadIdentity(); 
            GL.Ortho(0, _gameWindow.Width, 0, _gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);

            boardOnResize.Invoke(_gameWindow.Width, _gameWindow.Height);

            renderer.Clear();

            if (board != null)
                renderer.Add(board);

        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.LoadIdentity();

            renderer.Draw();

            mouse = Mouse.GetCursorState();

            mouseInWindowX = mouse.X - _gameWindow.Bounds.X - 8;
            mouseInWindowY = -mouse.Y + _gameWindow.Bounds.Bottom - 10;

            _gameWindow.SwapBuffers();
        }        

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
