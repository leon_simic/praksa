using System.Linq;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;

namespace OpenTk
{
    internal class Renderer
    {
        float deltaTime;
        public List<IDrawable> Drawables { get; private set; }
        public List<IUpdatable> Updatables { get; private set; }

        public Renderer(ref float deltaTime)
        {
            this.deltaTime = deltaTime;
            Drawables = new List<IDrawable>();
            Updatables = new List<IUpdatable>();
        }

        public void Add(IDrawable drawable)
        {
            Drawables.Add(drawable);
        }

        public void Add(IUpdatable updatable)
        {
            Updatables.Add(updatable);
        }
        public void Add(Board board)
        {
            Drawables.AddRange(board.Rings);
            Drawables.AddRange(board.Quads);
        }

        private void SortByLayer()
        {
            Drawables = Drawables.OrderBy(x => x.Layer).ToList();
        }

        public void Update()
        {
            foreach (var updatable in Updatables)
            {
                updatable.OnUpdate(ref deltaTime);
            }
        }

        public void Draw()
        {
            foreach (var drawable in Drawables)
            {
                GL.LoadIdentity();
                SortByLayer();
                drawable.Draw();
                GL.Color3(1f, 1f, 1f);
            }
        }
    }
}
