using OpenTK;

namespace OpenTk
{
    internal interface IUpdatable
    {
        void OnUpdate(ref float deltaTime);
    }
}
