using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace OpenTk
{
    internal class Ball : Circle, IUpdatable
    {
        public GameWindow GameWindow { get; set; }
        public bool TouchesBorder { get; private set; }


        public Ball(float radius, int segments, Vector position, Color color, GameWindow gameWindow) : base(radius, segments, position, color) 
        {
            GameWindow = gameWindow;
        }

        public void OnUpdate(ref float deltaTime)
        {
            GL.LoadIdentity();

            TouchesBorder = CheckIfTouchesBorder();

            if (!TouchesBorder)
                deltaTime += 1f / 60;
            else
            {
                if (Position.Y > Radius)
                    deltaTime -= 1f / 60;
                else
                    TouchesBorder = false;
            }

            Position = new Vector(Position.X, deltaTime * 60);
            Draw();
        }

        private bool CheckIfTouchesBorder()
        {
            if (Position.Y < GameWindow.Height - Radius && !TouchesBorder)
                return false;

            return true;
        }
    }
}
