namespace OpenTk
{
    internal interface IDrawable
    {
        int Layer { get; set; } 
        void Draw();
    }
}
