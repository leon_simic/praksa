using System;

namespace Graphic_Connect_4
{
    internal class AI
    {
        Game game;

        public AI(Game game)
        {
            this.game = game;   
        }

        public void PlayMove(Board board, ref Coin[,] coins, Renderer renderer, ref bool playerATurn)
        {
            WinnerChecker winnerChecker = new WinnerChecker(board);
            Random rand = new Random();
            int selectedColumn;
            Vector startPosition;

            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (!board.Rings[i][j].IsPopulated())
                    {
                        if (i == 0 || board.Rings[i - 1][j].IsPopulated())
                        {
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                            Coin tempCoinAI = new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);
                            coins[i, j] = tempCoinAI;
                            board.Rings[i][j].Populate(tempCoinAI);

                            if (winnerChecker.CheckForWin() == 2)
                            {
                                game.UpdatePlayerTurnText();

                                renderer.Add((IUpdatable)tempCoinAI);
                                playerATurn = !playerATurn;
                                return;
                            }
                            else
                            {
                                Coin tempCoinPlayer = new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow);

                                coins[i, j] = tempCoinPlayer;
                                board.Rings[i][j].Populate(tempCoinPlayer);

                                if (winnerChecker.CheckForWin() == 1)
                                {
                                    game.UpdatePlayerTurnText();

                                    coins[i, j] = tempCoinAI;
                                    board.Rings[i][j].Populate(tempCoinAI);
                                    renderer.Add((IUpdatable)tempCoinAI);
                                    playerATurn = !playerATurn;

                                    return;
                                }

                                coins[i, j] = null;
                                board.Rings[i][j].Clear();
                            }
                        }
                    }
                }
            }
            while (true)
            {
                selectedColumn = rand.Next(0, 7);

                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated() && !board.IsColumnFull(selectedColumn))
                        {
                            game.UpdatePlayerTurnText();

                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                            Coin tempCoinAI = new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            coins[i, j] = tempCoinAI;
                            board.Rings[i][j].Populate(tempCoinAI);
                            renderer.Add((IUpdatable)tempCoinAI);

                            playerATurn = !playerATurn;
                            return;
                        }
                    }
                }
            }
        }
    }
}
