using OpenTK;
using System;

namespace Graphic_Connect_4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GameWindow window = new GameWindow(600, 570);
            Game game = new Game(window);
            game.Run();
        }
    }
}
