using System;
using System.Collections.Generic;
using System.IO;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Graphic_Connect_4
{
    internal class Game
    {
        const int boardToWindowOffset = 80;

        delegate void OnResize(int width, int height);
        GameWindow gameWindow;
        Renderer renderer;
        
        Board board;
        bool boardVisible;
        Coin tempCoin;
        OnResize boardOnResize;
        WindowMouse mouse;
        List<bool>[] boardPopulation;
        Coin[,] coins;
        bool canClick;
        float coinToBoardOffset;

        bool playerATurn;
        bool onePlayerMode;
        bool gameInProgress;
        List<TextureQuad> startText;
        List<TextureQuad> endText;
        TextureQuad playerOnTurn;

        public Game(GameWindow gameWindow)
        {
            Initialize(gameWindow);
        }

        private void Initialize(GameWindow gameWindow)
        {
            mouse = new WindowMouse();

            playerATurn = true;
            onePlayerMode = false;
            gameInProgress = false;

            canClick = true;
            float deltaTime = 1f / 60;
            this.gameWindow = gameWindow;
            renderer = new Renderer(deltaTime);

            board = new Board(new Vector(this.gameWindow.Width / 2, (this.gameWindow.Height - boardToWindowOffset) / 2));
            boardVisible = false;
            boardOnResize = new OnResize(board.OnResize);

            coins = new Coin[board.Rows, board.Columns];

            boardPopulation = new List<bool>[board.Rows];
            for (int i = 0; i < board.Rows; i++)
            {
                boardPopulation[i] = new List<bool> { false, false, false, false, false, false, false };
            }

            CreateStartText();
            DrawStartAndEndText();
        }

        private void CreateStartText()
        {
            Texture startTextTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "ClickOnGameMode.jpg"));
            Texture onePlayerModeTextTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "OnePlayerMode.jpg"));
            Texture twoPlayerModeTextTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "TwoPlayerMode.jpg"));

            startText = new List<TextureQuad>();
            TextureQuad startTextLine = new TextureQuad(600, 80, new Vector(300, gameWindow.Height - 20), startTextTexture);
            startText.Add(startTextLine);
            TextureQuad onePlayerModeText = new TextureQuad(230, 70, new Vector(115, startText[0].Position.Y - 50), onePlayerModeTextTexture);
            startText.Add(onePlayerModeText);
            TextureQuad twoPlayerModeText = new TextureQuad(230, 70, new Vector(115, startText[1].Position.Y - 50), twoPlayerModeTextTexture);
            startText.Add(twoPlayerModeText); 
        }

        public void UpdatePlayerTurnText()
        {
            if (renderer.Contains(playerOnTurn))
                renderer.Remove(playerOnTurn);

            if (!onePlayerMode)
            {
                if (!playerATurn)
                {
                    Texture playerATurnTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "PlayerATurn.jpg"));
                    playerOnTurn = new TextureQuad(420, 40, new Vector(gameWindow.Width / 2, gameWindow.Height - 50), playerATurnTexture);
                }
                else
                {
                    Texture playerBTurnTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "PlayerBTurn.jpg"));
                    playerOnTurn = new TextureQuad(420, 40, new Vector(gameWindow.Width / 2, gameWindow.Height - 50), playerBTurnTexture);
                }
            }
            else
            {
                if (!playerATurn)
                {
                    Texture yourTurnTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "YourTurn.jpg"));
                    playerOnTurn = new TextureQuad(420, 40, new Vector(gameWindow.Width / 2, gameWindow.Height - 50), yourTurnTexture);
                }
                else
                {
                    Texture AITurnTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "AITurn.jpg"));
                    playerOnTurn = new TextureQuad(420, 40, new Vector(gameWindow.Width / 2, gameWindow.Height - 50), AITurnTexture);
                }
            }

            renderer.Add(playerOnTurn);
        }

        private void CreateEndText(int winner)
        {
            Texture winnerATexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "WinnerA.jpg"));
            Texture winnerBTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "WinnerB.jpg"));
            Texture winnerAITexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "WinnerAI.jpg"));
            Texture youWinnerTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "YouWin.jpg"));
            Texture tieTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "Tie.jpg"));
            Texture answerYesTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "AnswerYes.jpg"));
            Texture answerNoTexture = new Texture(Path.Combine(Environment.CurrentDirectory, @"resources\\", "AnswerNo.jpg"));

            endText = new List<TextureQuad>();

            if (winner == 1)
            {
                if (!onePlayerMode)
                {
                    TextureQuad winnerA = new TextureQuad(500, 100, new Vector(240, gameWindow.Height - 50), winnerATexture);
                    endText.Add(winnerA);
                }
                else
                {
                    TextureQuad youWinner = new TextureQuad(500, 100, new Vector(240, gameWindow.Height - 50), youWinnerTexture);
                    endText.Add(youWinner);
                }
            }
            else if (winner == 2)
            {
                if (!onePlayerMode)
                {
                    TextureQuad winnerB = new TextureQuad(500, 100, new Vector(240, gameWindow.Height - 50), winnerBTexture);
                    endText.Add(winnerB);
                }
                else
                {
                    TextureQuad winnerAI = new TextureQuad(500, 100, new Vector(240, gameWindow.Height - 50), winnerAITexture);
                    endText.Add(winnerAI);
                }
            }
            else if (winner == 0)
            {
                TextureQuad tie = new TextureQuad(500, 100, new Vector(240, gameWindow.Height - 50), tieTexture);
                endText.Add(tie);
            }

            TextureQuad answerYes = new TextureQuad(60, 40, new Vector(endText[0].Position.X + 230, endText[0].Position.Y - 20), answerYesTexture);
            endText.Add(answerYes);
            TextureQuad answerNo = new TextureQuad(60, 40, new Vector(endText[1].Position.X + 70, endText[1].Position.Y), answerNoTexture);
            endText.Add(answerNo);

            foreach (var line in endText)
            {
                renderer.Add(line);
            } 
        }

        public void Run()
        {
            gameWindow.Load += LoadedEventHandler; 
            gameWindow.Resize += ResizeEventHandler;
            gameWindow.RenderFrame += RenderFrameEventHandler;
            gameWindow.MouseUp += MouseUpEventHandler;
            gameWindow.Run();
        }
         
        private void DeleteStartText()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);

            foreach (var quad in startText)
            {
                GL.DeleteTexture(quad.Texture.Id);
            }

            startText.Clear();
            startText.Capacity = 0;

            GL.ClearColor(0, 0, 0, 0);
            GL.LoadIdentity();
        }

        private void DeleteEndText()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);

            foreach (var quad in endText)
            {
                GL.DeleteTexture(quad.Texture.Id);
            }

            endText.Clear();
            endText.Capacity = 0;

            GL.ClearColor(0, 0, 0, 0);
            GL.LoadIdentity();
        }

        private void MouseUpEventHandler(object sender, MouseButtonEventArgs e)
        {
            if (gameInProgress)
            {
                if (onePlayerMode)
                    StartOnePlayerMode();
                else
                    StartTwoPlayerModeMove();
            }
            else if (PickedStartAgain())
            {
                StartAgain();
            }
            else if (PickedGameOver())
            {
                gameWindow.Close();
            }

            if (PickedOnePlayerMode())
                InitializeOnePlayerMode();
            else if (PickedTwoPlayerMode())
                InitializeTwoPlayerMode();
        }

        private bool PickedGameOver()
        {
            if (endText != null && endText.Count != 0)
                if (endText[2].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private void StartAgain()
        {
            renderer.Clear();
            DeleteEndText();
            boardVisible = true; 
            Initialize(gameWindow);
        }

        private bool PickedStartAgain()
        {
            if (endText != null && endText.Count != 0)
                if (endText[1].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        } 

        private void InitializeTwoPlayerMode()
        {
            onePlayerMode = false;
            gameInProgress = true;
            boardVisible = true;

            foreach (var quad in startText)
            {
                renderer.Remove(quad);
            }
            DeleteStartText();

            playerATurn = false;
            UpdatePlayerTurnText();
            playerATurn = true;
        }

        private void InitializeOnePlayerMode()
        {
            onePlayerMode = true;
            gameInProgress = true;
            boardVisible = true;

            foreach (var quad in startText)
            {
                renderer.Remove(quad);
            }
            DeleteStartText();

            playerATurn = false;
            UpdatePlayerTurnText();
            playerATurn = true;
        }

        private bool PickedOnePlayerMode()
        {
            if (startText.Count != 0 && !gameInProgress)
                if (startText[1].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private bool PickedTwoPlayerMode()
        {
            if (startText.Count != 0 && !gameInProgress)
                if (startText[2].Contains(mouse.X, mouse.Y))
                    return true;

            return false;
        }

        private void StartTwoPlayerModeMove()
        {
            boardVisible = true;

            PlayerMove();
        }

        private void PlayerMove()
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;
            if (canClick)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouse.X, mouse.Y))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated())
                        {
                            UpdatePlayerTurnText();

                            tempCoin = playerATurn ?
                                new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow) :
                                new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            renderer.Add((IUpdatable)tempCoin);
                            coins[i, j] = tempCoin;
                            board.Rings[i][j].Populate(tempCoin);

                            playerATurn = !playerATurn;
                            return;
                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void StartOnePlayerMode()
        {
            AI AI = new AI(this);
            boardVisible = true;

            if (canClick)
            {
                if (playerATurn)
                {
                    PlayerMove();
                }
                else
                {
                    AI.PlayMove(board, ref coins, renderer, ref playerATurn);
                }
            }
        }
        

        private void CreateCopyBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    boardPopulation[i][j] = board.Rings[i][j].IsPopulated();
                }
            }

        }

        private void SetBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    board.Rings[i][j].Populate(coins[i, j]);
                }
            }
        }

        private void DrawStartAndEndText()
        {
            if (startText.Count != 0)
            {
                foreach (var line in startText)
                {
                    renderer.Add(line);
                }
            }

            if (endText != null)
            {
                foreach (var line in endText)
                {
                    renderer.Add(line);
                }
            }
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit); 
            GL.LoadIdentity();

            if (boardVisible && !renderer.Contains(board))
                renderer.Add(board);

            renderer.Update();
            renderer.Draw(); 

            if (gameInProgress)
                UpdatePlayerTurnTextPosition();

            CheckForWin();

            mouse.SetMouseCoordinates(gameWindow);

            gameWindow.SwapBuffers();
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, gameWindow.Width, gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, gameWindow.Width, 0, gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);

            if (board.Rings != null)
                CreateCopyBoardPopulation();
    
            UpdateStartTextPosition();
            UpdateEndTextPosition();

            renderer.Clear();
            DrawStartAndEndText();

            SetCoinToBoardOffset();
            boardOnResize.Invoke(gameWindow.Width, gameWindow.Height - boardToWindowOffset);

            SetBoardPopulation();
            UpdateCoinsPosition();
        }

        private void UpdateStartTextPosition()
        {
            if (startText.Count != 0)
            {
                startText[0].Position = new Vector(300, gameWindow.Height - 20);
                startText[1].Position = new Vector(115, startText[0].Position.Y - 50);
                startText[2].Position = new Vector(115, startText[1].Position.Y - 50);
            }
        }

        private void UpdateEndTextPosition()
        {
            if (endText != null && endText.Count != 0)
            {
                endText[0].Position = new Vector(240, gameWindow.Height - 50);
                endText[1].Position = new Vector(endText[0].Position.X + 230, endText[0].Position.Y - 20);
                endText[2].Position = new Vector(endText[1].Position.X + 70, endText[1].Position.Y);
            }
        }

        private void UpdatePlayerTurnTextPosition()
        {
            if (playerOnTurn != null)
            {
                renderer.Remove(playerOnTurn);
                playerOnTurn.Position = new Vector(gameWindow.Width / 2, gameWindow.Height - 50);
                renderer.Add(playerOnTurn);
            }
        }

        private void UpdateCoinsPosition()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (!coins[i, j].IsMoving)
                        {
                            coins[i, j].Position = board.Rings[i][j].Position;
                            renderer.Add((IDrawable)coins[i, j]);
                        }
                        else
                        {
                            coins[i, j].Position = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y + coinToBoardOffset);
                            coins[i, j].EndPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y);
                            renderer.Add((IUpdatable)coins[i, j]);
                        }
                    }
                }
            }
        }

        private void SetCoinToBoardOffset()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null && coins[i, j].IsMoving)
                        coinToBoardOffset = coins[i, j].Position.Y - board.Rings[0][0].Position.Y;
                }
            }
        }

        private void CheckForWin()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (coins[i, j].IsMoving)
                        {
                            canClick = false;
                            return;
                        }   
                    }
                }
            }
            canClick = true;

            CheckTheWinner();
        }

        private void CheckTheWinner()
        {
            if (board.Rings != null)
            {
                WinnerChecker winnerChecker = new WinnerChecker(board);
                int winner = winnerChecker.CheckForWin();

                if (winner != 0)
                {
                    gameInProgress = false;
                    boardVisible = true;

                    renderer.Remove(playerOnTurn);
                    CreateEndText(winner);
                }
                else if (board.IsFull())
                {
                    gameInProgress = false;
                    boardVisible = true;

                    renderer.Remove(playerOnTurn);
                    CreateEndText(0);
                }
            }
        }

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}