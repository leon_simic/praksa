using System;

namespace Graphic_Connect_4
{
    enum PlayerType
    {
        PlayerA,
        PlayerB
    }

    internal class Coin : Circle, IUpdatable
    {
        public float PositionOffset { get; set; }
        public Vector EndPosition { get; set; }
        public bool IsMoving { get; private set; }
        public PlayerType Type { get; private set; }


        public Coin(PlayerType type, float radius, int segments, Vector startPosition, Vector endPosition, Color color) : base(radius, segments, startPosition, color) 
        {
            Type = type;
            PositionOffset = 1000;
            EndPosition = endPosition;
            Layer = 1;
        }

        public void OnUpdate(float deltaTime)
        {
            if (Position != EndPosition)
            { 
                Position = new Vector(Position.X, (int)(Position.Y - deltaTime * PositionOffset));
                IsMoving = true;
            }
            if (Math.Pow(EndPosition.X - Position.X, 2) + Math.Pow(EndPosition.Y - Position.Y, 2) <= Math.Pow(deltaTime * PositionOffset, 2) 
                || Position == EndPosition)
            {
                Position = EndPosition;
                IsMoving = false;
            }
            Draw();
        }
    }
}
