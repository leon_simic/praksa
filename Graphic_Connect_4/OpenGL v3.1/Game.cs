using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Graphic_Connect_4
{
    internal class Game
    {     
        delegate void OnResize(int width, int height);
        GameWindow _gameWindow;
        public GameWindow MainWindow => _gameWindow;
        Renderer renderer;
        
        Board board; 
        Coin tempCoin;
        OnResize boardOnResize;
        MouseState mouse;
        List<bool>[] boardPopulation;
        Coin[,] coins;
        bool canClick;
        float coinToBoardOffset;

        float mouseInWindowX;
        float mouseInWindowY;

        bool playerATurn;
        bool onePlayerMode;
        bool gameStarted;

        Texture crate;
        Texture brick;
        List<TextureQuad> textureQuads;

        public Game(GameWindow gameWindow)
        {
            playerATurn = true;
            onePlayerMode = false;
            gameStarted = false;
            
            canClick = true;
            float deltaTime = 1f / 60;
            _gameWindow = gameWindow;
            renderer = new Renderer(deltaTime);

            board = new Board(new Vector(_gameWindow.Width / 2, _gameWindow.Height / 2));
            boardOnResize = new OnResize(board.OnResize);

            coins = new Coin[board.Rows, board.Columns];
                        
            boardPopulation = new List<bool>[board.Rows];
            for (int i = 0; i < board.Rows; i++)
            {
                boardPopulation[i] = new List<bool> { false, false, false, false, false, false, false};
            }

            Texture startText = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\ClickOnGameMode.jpg");
            Texture onePlayerModeText = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\OnePlayerMode.jpg");
            Texture twoPlayerModeText = new Texture(@"C:\Users\Reroot\source\repos\Graphic_Connect_4\TwoPlayerMode.jpg");

            textureQuads = new List<TextureQuad>();
            TextureQuad startTextQuad = new TextureQuad(600, 80, new Vector(300, _gameWindow.Height - 20), startText);
            textureQuads.Add(startTextQuad);
            TextureQuad onePlayerModeTextQuad = new TextureQuad(230, 70, new Vector(115, startTextQuad.Position.Y - 50), onePlayerModeText);
            textureQuads.Add(onePlayerModeTextQuad);
            TextureQuad twoPlayerModeTextQuad = new TextureQuad(230, 70, new Vector(115, onePlayerModeTextQuad.Position.Y - 50), twoPlayerModeText);
            textureQuads.Add(twoPlayerModeTextQuad);

        }
          
        public void Practice()
        {
            float deltaTime = 1f / 60;
            renderer = new Renderer(deltaTime);

            crate = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\crateTexture.bmp");
            brick = new Texture(@"C:\Users\Reroot\source\repos\OpenTk\brickTexture.bmp");

            Triangle triangle = new Triangle(30, new Vector(400, 400), Color.Red);
            triangle.Layer = 1;
            renderer.Add(triangle);

            Circle circle = new Circle(100, 40, new Vector(100, 100), Color.Green);
            circle.Layer = 1;
            renderer.Add(circle);

            Ring ring = new Ring(100, 50, 40, new Vector(300, 200), Color.Blue);
            ring.Layer = 1;
            renderer.Add(ring);

            Quad quad = new Quad(50, 150, new Vector(500, 200), Color.Red);
            quad.Layer = 1;
            renderer.Add(quad);

            TextureQuad crateQuad = new TextureQuad(200, 100, new Vector(500, 70), crate);
            crateQuad.Layer = 1;
            renderer.Add(crateQuad);

            TextureQuad brickQuad = new TextureQuad(100, 98, new Vector(300, 70), brick);
            brickQuad.Layer = 1;
            renderer.Add(brickQuad);
        }

        private void Run()
        {
            _gameWindow.Load += LoadedEventHandler; 
            _gameWindow.Resize += ResizeEventHandler;
            _gameWindow.RenderFrame += RenderFrameEventHandler;
            _gameWindow.MouseUp += MouseUpEventHandler;
            _gameWindow.Run();
        }

        public void Start()
        {
            bool playAgain = true;
            bool validInputGameMode;
            Run();


            //do
            //{
            //    Console.WriteLine("Enter which game mode you want to play. (1/2)");
            //    Console.WriteLine("1 - 1 player");
            //    Console.WriteLine("2 - 2 players");
            //    validInputGameMode = int.TryParse(Console.ReadLine(), out int gameMode);

            //    if (!validInputGameMode || gameMode < 1 || gameMode > 2)
            //    {
            //        Console.Clear();
            //        Console.WriteLine("Invalid input.");
            //        continue;
            //    }

            //    if (gameMode == 1)
            //    {
            //        onePlayerMode = true;
            //    }
            //    else
            //    {
            //        onePlayerMode = false;
            //    }

            //} while (playAgain);
        }

        private void DeleteText()
        {
            GL.BindTexture(TextureTarget.Texture2D, 0);

            foreach (var quad in textureQuads)
            {
                GL.DeleteTexture(quad.Texture.Id);
            }

            textureQuads.Clear();
            textureQuads.Capacity = 0;

            GL.ClearColor(0, 0, 0, 0);
        }

        private void MouseUpEventHandler(object sender, MouseButtonEventArgs e)
        {
            if (textureQuads.Count != 0 && !gameStarted)
            {
                if (textureQuads[1].Contains(mouseInWindowX, mouseInWindowY))
                {
                    Console.WriteLine("prvi");
                    onePlayerMode = true;
                    DeleteText();
                    gameStarted = true;
                }
                else if (textureQuads[2].Contains(mouseInWindowX, mouseInWindowY))
                {
                    Console.WriteLine("drugi");
                    onePlayerMode = false;
                    DeleteText();
                    gameStarted = true;
                }
            }

            if (onePlayerMode)
                OnePlayerModeMove();
            else
                TwoPlayerModeMove();
        }

        private void TwoPlayerModeMove()
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;
         
            if (canClick)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouseInWindowX, mouseInWindowY))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated())
                        {
                            tempCoin = playerATurn ?
                                new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow) :
                                new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            renderer.Add((IUpdatable)tempCoin);
                            coins[i, j] = tempCoin;
                            board.Rings[i][j].Populate(tempCoin);

                            playerATurn = !playerATurn;
                            return;
                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void OnePlayerModeMove()
        {
            Vector startPosition = new Vector();
            int selectedColumn = -1;

            if (canClick)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (board.Rings[i][j].Contains(mouseInWindowX, mouseInWindowY))
                        {
                            selectedColumn = j;
                            startPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[board.Rows - 1][board.Columns - 1].Position.Y);
                        }
                    }
                }
            }

            if (selectedColumn != -1)
            {
                for (int i = 0; i < board.Rows; i++)
                {
                    for (int j = 0; j < board.Columns; j++)
                    {
                        if (j == selectedColumn && !board.Rings[i][j].IsPopulated())
                        {
                            tempCoin = playerATurn ?
                                new Coin(PlayerType.PlayerA, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Yellow) :
                                new Coin(PlayerType.PlayerB, 20, 50, startPosition, new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y), Color.Red);

                            renderer.Add((IUpdatable)tempCoin);
                            board.Rings[i][j].Populate(tempCoin);
                            coins[i, j] = tempCoin;

                            playerATurn = !playerATurn;
                            return;
                        }
                        else if (j == selectedColumn && i == board.Rows - 1)
                            return;
                    }
                }
            }
        }

        private void CreateCopyBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    boardPopulation[i][j] = board.Rings[i][j].IsPopulated();
                }
            }

        }

        private void SetBoardPopulation()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    board.Rings[i][j].Populate(coins[i, j]);
                }
            }
        }

        private void ResizeEventHandler(object o, EventArgs eventArgs)
        {
            GL.Viewport(0, 0, _gameWindow.Width, _gameWindow.Height);
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, _gameWindow.Width, 0, _gameWindow.Height, -1.0, 1.0);
            GL.MatrixMode(MatrixMode.Modelview);

            if (board.Rings != null)
                CreateCopyBoardPopulation();
            
            if (!gameStarted)
                boardOnResize.Invoke(_gameWindow.Width, _gameWindow.Height);

            renderer.Clear();

            foreach (var quad in textureQuads)
            {
                renderer.Add(quad);
            }

            SetBoardPopulation();
            UpdateCoinsPosition();
            SetCoinToBoardOffset();
        }

        private void UpdateCoinsPosition()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (!coins[i, j].IsMoving)
                        {
                            coins[i, j].Position = board.Rings[i][j].Position;
                            renderer.Add((IDrawable)coins[i, j]);
                        }
                        else
                        {
                            coins[i, j].Position = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y + coinToBoardOffset);
                            coins[i, j].EndPosition = new Vector(board.Rings[i][j].Position.X, board.Rings[i][j].Position.Y);
                            renderer.Add((IUpdatable)coins[i, j]);
                        }
                    }
                }
            }
        }

        private void SetCoinToBoardOffset()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null && coins[i, j].IsMoving)
                        coinToBoardOffset = coins[i, j].Position.Y - board.Rings[i][j].Position.Y;
                }
            }
        }

        private void RenderFrameEventHandler(object o, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit); 
            GL.LoadIdentity();

            if (gameStarted && !renderer.Contains(board))
                renderer.Add(board);

            if (gameStarted)
                renderer.Update();

            renderer.Draw();

            CheckForWin();
            SetMouseCoordinates();

            _gameWindow.SwapBuffers();
        }        

        private void SetMouseCoordinates()
        {
            mouse = Mouse.GetCursorState();
            mouseInWindowX = mouse.X - _gameWindow.Bounds.X - 8;
            mouseInWindowY = -mouse.Y + _gameWindow.Bounds.Bottom - 10;
        }

        private void CheckForWin()
        {
            for (int i = 0; i < board.Rows; i++)
            {
                for (int j = 0; j < board.Columns; j++)
                {
                    if (coins[i, j] != null)
                    {
                        if (coins[i, j].IsMoving)
                        {
                            canClick = false;
                            return;
                        }
                        else
                            CheckTheWinner();
                    }
                }
            }
            canClick = true;
        }

        private void CheckTheWinner()
        {
            WinnerChecker winnerChecker = new WinnerChecker(board);
            int winner = winnerChecker.CheckForWin();

            if (winner != 0)
            {
                //_gameWindow.Close();
                Console.WriteLine("The winner is: " + tempCoin.Type);

                if (IsGameOver())
                    return;
                else
                    return;
            }
            else if (board.IsFull())
            {
                //_gameWindow.Close();
                Console.WriteLine("It's a tie!");

                if (IsGameOver())
                    return;
                else
                    return;
            }
        }
        
        public static bool IsGameOver()
        {
            bool validInput;
            char input;

            do
            {
                Console.WriteLine("Do you want to play again? (y/n): ");
                validInput = char.TryParse(Console.ReadLine(), out input);
                input = char.ToLower(input);
            } while ((input != 'n' && input != 'y') || !validInput);

            if (input == 'y')
                return false;

            else
                return true;
        }

        private void LoadedEventHandler(object o, EventArgs e)
        {
            GL.ClearColor(0, 0, 0, 0);
        }
    }
}
