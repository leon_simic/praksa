using OpenTK;
using System;

namespace OpenTk
{
    internal class Program
    {
        static void Main(string[] args)
        {
            GameWindow practice = new GameWindow();
            Window window = new Window(practice);
        }
    }
}
